﻿/*
 * MIT License (MIT) - Copyright (c) 2022 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using static Vulkan.API;

namespace Vulkan;


/// <summary>
/// Interface base type for all Vulkan object handle types.
/// </summary>
#if VVK_API_INTERNAL
internal
#else
public
#endif
unsafe interface IVulkanHandle
{
	/// <summary>The object type of the handle.</summary>
	static abstract VkObjectType ObjectType { get; }

	/// <summary>The raw handle pointer for the object.</summary>
	void* RawHandle { get; }
}
