﻿/*
 * MIT License (MIT) - Copyright (c) 2022 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace Vulkan;


/// <summary>
/// UTF8-encoded native string data.
/// </summary>
[StructLayout(LayoutKind.Explicit, Size=8)]
[DebuggerDisplay("String = {ToString()}")]
#if VVK_API_INTERNAL
internal
#else
public
#endif
unsafe sealed class NativeStringUTF8 : IDisposable
{
	#region Fields
	/// <summary>The raw string data, null-terminated. This data should not be manipulated directly.</summary>
	public byte* Data => _data;
	[FieldOffset(0)] private byte* _data = null;

	/// <summary>The length of the string, in characters.</summary>
	public uint CharCount => CountChar(_data);

	/// <summary>The size of the string data, in bytes, without the null terminator.</summary>
	public uint DataSize => StrLen(_data);
	#endregion // Fields

	/// <summary>Construct an empty string.</summary>
	public NativeStringUTF8()
	{
		_data = ReserveMemory(0);
		_data[0] = 0;
	}
	/// <summary>Construct a native string from a given managed string.</summary>
	public NativeStringUTF8(string str) =>
		_data = CreateNativeString(str, out _);

	/// <summary>Construct a native string by copying in an existing UTF8-encoded native string.</summary>
	/// <param name="data">The existing UTF-8 string.</param>
	/// <param name="length">The string length (in bytes), or null to auto-calculate.</param>
	public NativeStringUTF8(byte* data, uint? length = null) =>
		_data = CreateNativeString(ToManagedString(data, length), out _);

	~NativeStringUTF8()
	{
		dispose();
	}

	public override string ToString() =>
		(_data != null) ? Encoding.UTF8.GetString(_data, (int)StrLen(_data)) : "";

	#region IDisposable
	public void Dispose()
	{
		dispose();
		GC.SuppressFinalize(this);
	}

	private void dispose()
	{
		if (_data != null) {
			Marshal.FreeHGlobal(new IntPtr(_data));
		}
		_data = null;
	}
	#endregion // IDisposable

	/// <summary>Gets a managed string from a raw utf-8 encoded string.</summary>
	public static string ToManagedString(byte* str, uint? length = null)
	{
		if (str == null) {
			return "";
		}
		if (!length.HasValue) {
			length = StrLen(str);
		}
		return Encoding.UTF8.GetString(str, (int)length.Value);
	}

	// Reserve enough memory for the given byte count + null terminator
	private static byte* ReserveMemory(uint byteCount) =>
		(byte*)Marshal.AllocHGlobal((int)(byteCount + 1u)).ToPointer();

	// Puts the string data into a newly allocated part of memory
	internal static byte* CreateNativeString(string str, out uint len)
	{
		var byteCount = Encoding.UTF8.GetByteCount(str);
		var data = ReserveMemory((uint)byteCount);
		Encoding.UTF8.GetBytes(str.AsSpan(), new Span<byte>(data, byteCount));
		data[byteCount] = 0;
		len = (uint)byteCount;
		return data;
	}

	// Get the number of UTF-8 characters in the string
	internal static uint CountChar(byte* data)
	{
		if (data == null) {
			return 0;
		}
		int bi = 0, cc = 0;
		while (data[bi] != 0) {
			if ((data[bi] & 0xC0) != 0x80) {
				++cc;
			}
			++bi;
		}
		return (uint)cc;
	}

	// Standard C strlen
	internal static uint StrLen(byte* data)
	{
		if (data == null) {
			return 0;
		}
		int bi = 0;
		while (data[bi++] != 0) ;
		return (uint)bi - 1;
	}
}
