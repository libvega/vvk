﻿/*
 * MIT License (MIT) - Copyright (c) 2022 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace Vulkan;


/// <summary>
/// Utility functionality for <see cref="API.VkResult"/> values.
/// </summary>
#if VVK_API_INTERNAL
internal
#else
public
#endif
static class VkResultUtils
{
	/// <summary>Gets if the result represents an error.</summary>
	public static bool IsError(this API.VkResult result) => (int)result < 0;

	/// <summary>Gets if the result is VK_SUCCESS (<see cref="API.VkResult.Success"/>).</summary>
	public static bool IsSuccess(this API.VkResult result) => result == API.VkResult.Success;

	/// <summary>Gets if the result is a non-error value, representing a success or some other status.</summary>
	public static bool IsStatus(this API.VkResult result) => (int)result >= 0;

	/// <summary>Throws an exception if the result code is an error code. <c>DEBUG</c> builds only.</summary>
	/// <param name="result">The result code to throw for.</param>
	/// <param name="message">An optional message to include in the exception.</param>
	/// <param name="memberName">Compiler generated, do not populate.</param>
	/// <param name="sourceFile">Compiler generated, do not populate.</param>
	/// <param name="sourceLine">Compiler generated, do not populate.</param>
	[MethodImpl(MethodImplOptions.AggressiveInlining)]
	public static void ThrowIfError(
		this API.VkResult result, 
		string? message = null,
		[CallerMemberName] string memberName = "",
		[CallerFilePath] string sourceFile = "",
		[CallerLineNumber] int sourceLine = 0
	)
	{
		if (IsError(result)) {
			sourceFile = sourceFile.Split(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)[^1];
			var prefix = $"{result} ({sourceFile}:{sourceLine}@{memberName}) - ";
			throw new VkResultException(result, prefix + (message ?? "error"));
		}
	}
}


/// <summary>
/// Exception thrown when <see cref="VkResultUtils.ThrowIfError"/> encounters an error code.
/// </summary>
#if VVK_API_INTERNAL
internal
#else
public
#endif
sealed class VkResultException : Exception
{
	/// <summary>The error code generating the exception.</summary>
	public readonly API.VkResult Result;

	public VkResultException(API.VkResult result, string message) : base(message) => Result = result;
}
