﻿/*
 * MIT License (MIT) - Copyright (c) 2022 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace Vulkan;


// Contains additional utility functionality used internally by the API types
internal unsafe static class InternalUtilities
{
	// Perform a fast memcmp using SIMD acceleration if possible
	[MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
	public static bool Memcmp<T>(T* left, T* right, ulong count)
		where T : unmanaged
	{
		uint VECTOR_LEN = (uint)Vector<byte>.Count;
		byte* leftRaw = (byte*)left, rightRaw = (byte*)right;
		long length = (long)count * sizeof(T);

		bool result = false;
		if (Vector.IsHardwareAccelerated) {
			while (length >= VECTOR_LEN) {
				if (Unsafe.Read<Vector<byte>>(leftRaw) == Unsafe.Read<Vector<byte>>(rightRaw)) {
					leftRaw += VECTOR_LEN;
					rightRaw += VECTOR_LEN;
					length -= VECTOR_LEN;
				}
				else 
					goto end;
			}
		}
		while (length >= sizeof(ulong)) {
			if (Unsafe.Read<ulong>(leftRaw) == Unsafe.Read<ulong>(rightRaw)) {
				leftRaw += sizeof(ulong);
				rightRaw += sizeof(ulong);
				length -= sizeof(ulong);
			}
			else
				goto end;
		}
		while (length >= sizeof(byte)) {
			if (Unsafe.Read<byte>(leftRaw) == Unsafe.Read<byte>(rightRaw)) {
				leftRaw += sizeof(byte);
				rightRaw += sizeof(byte);
				length -= sizeof(byte);
			}
			else
				goto end;
		}

		result = true;
	end:
		return result;
	}

	// Gets a thread-safe cached StringBuilder for use in the API struct ToString() methods
	private static ThreadLocal<StringBuilder> _CachedStringBuilder = new(() => new());
	public static StringBuilder GetCachedThreadStringBuilder()
	{
		var sb = _CachedStringBuilder.Value!;
		sb.Clear();
		return sb;
	}
}
