﻿/*
 * MIT License (MIT) - Copyright (c) 2022 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Runtime.InteropServices;

namespace Vulkan;


/// <summary>
/// Contains the generated Vulkan API.
/// </summary>
/// <remarks>Designed to be used as <c>using static Vulkan.API;</c> to import the Vulkan API into the namespace.</remarks>
#if VVK_API_INTERNAL
internal
#else
public
#endif
static unsafe partial class API
{
	#region Macro Constants
	/// <summary>Version constant for API version 1.0.*.</summary>
	public static readonly uint VK_API_VERSION_1_0 = VK_MAKE_VERSION(1, 0, 0);
	/// <summary>Version constant for API version 1.1.*.</summary>
	public static readonly uint VK_API_VERSION_1_1 = VK_MAKE_VERSION(1, 1, 0);
	/// <summary>Version constant for API version 1.2.*.</summary>
	public static readonly uint VK_API_VERSION_1_2 = VK_MAKE_VERSION(1, 2, 0);

	/// <summary>Constant defining a null object handle.</summary>
	public static readonly void* VK_NULL_HANDLE = null;
	#endregion // Macro Constants

	#region Version
	/// <summary>Implementation of the <c>VK_MAKE_VERSION(major,minor,patch)</c> macro.</summary>
	public static uint VK_MAKE_VERSION(uint major, uint minor, uint patch) => (major << 22) | (minor << 12) | patch;

	/// <summary>Implementation of the <c>VK_VERSION_MAJOR(version)</c> macro.</summary>
	public static uint VK_VERSION_MAJOR(uint version) => version >> 22;

	/// <summary>Implementation of the <c>VK_VERSION_MINOR(version)</c> macro.</summary>
	public static uint VK_VERSION_MINOR(uint version) => (version >> 12) & 0x3FFu;

	/// <summary>Implementation of the <c>VK_VERSION_PATCH(version)</c> macro.</summary>
	public static uint VK_VERSION_PATCH(uint version) => version & 0xFFFu;

	/// <summary>Decomposes the version value into its major, minor, and patch parts.</summary>
	public static (uint Major, uint Minor, uint Patch) VK_VERSION_UNPACK(uint version) =>
		(version >> 22, (version >> 12) & 0x3FFu, version & 0xFFFu);
	#endregion // Version

	/// <summary>
	/// 32-bit integer value used by the Vulkan API for booleans.
	/// </summary>
	[StructLayout(LayoutKind.Explicit, Size=4)]
	public readonly struct VkBool32 : IEquatable<VkBool32>, IEquatable<bool>
	{
		#region Fields
		[FieldOffset(0)] private readonly uint _value = API.VK_FALSE;

		/// <summary>Constant true boolean value.</summary>
		public static readonly VkBool32 False = new(API.VK_FALSE);
		/// <summary>Constant true boolean value.</summary>
		public static readonly VkBool32 True = new(API.VK_TRUE);
		#endregion // Fields

		/// <summary>Construct a Vulkan boolean value from an integer (0 = false, non-zero = 1 = true).</summary>
		public VkBool32(uint raw) => _value = (raw == API.VK_FALSE) ? API.VK_FALSE : API.VK_TRUE;

		/// <summary>Construct a Vk boolean from a regular boolean.</summary>
		public VkBool32(bool value) => _value = value ? API.VK_TRUE : API.VK_FALSE;

		#region Overrides
		public readonly override string ToString() => _value.ToString();
	
		public readonly override int GetHashCode() => _value.GetHashCode();
	
		public readonly override bool Equals(object? o) => (o is VkBool32 obj) && (obj._value == _value);

		public readonly bool Equals(VkBool32 o) => o._value == _value;

		public readonly bool Equals(bool b) => (_value == API.VK_TRUE) == b;
		#endregion // Overrides

		#region Operators
		public static implicit operator bool (VkBool32 b) => b._value == API.VK_TRUE;

		public static explicit operator uint (VkBool32 b) => b._value;

		public static explicit operator int (VkBool32 b) => (int)b._value;

		public static implicit operator VkBool32 (bool b) => new VkBool32(b);

		public static implicit operator VkBool32 (uint v) => new VkBool32(v);

		public static implicit operator VkBool32 (int v) => new VkBool32((uint)v);

		public static bool operator == (VkBool32 l, VkBool32 r) => l._value == r._value;

		public static bool operator != (VkBool32 l, VkBool32 r) => l._value != r._value;

		public static VkBool32 operator & (VkBool32 l, VkBool32 r) => new VkBool32(l._value & r._value);

		public static VkBool32 operator | (VkBool32 l, VkBool32 r) => new VkBool32(l._value | r._value);

		public static VkBool32 operator ^ (VkBool32 l, VkBool32 r) => new VkBool32(l._value ^ r._value);
		#endregion // Operators
	}

	static API()
	{
		LoadGlobals();
	}
}
