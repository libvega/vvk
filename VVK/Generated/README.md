## Generated

This directory is the official output location for the generated API files. All files in this directory are ignored by version control (except for this README).

The project file for VVK has a pre-build step to generate the API files using the Generator project.
