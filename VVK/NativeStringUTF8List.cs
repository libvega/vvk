﻿/*
 * MIT License (MIT) - Copyright (c) 2022 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Vulkan;


/// <summary>
/// Represents a contiguous list of native utf-8 encoded strings. Designed to act like a <c>char**</c> value.
/// </summary>
#if VVK_API_INTERNAL
internal
#else
public
#endif
unsafe sealed class NativeStringUTF8List : IDisposable
{
	private const uint DEFAULT_CAPACITY = 16;

	#region Fields
	/// <summary>The raw array of string pointers. Do not manipulate this data directly.</summary>
	public byte** Data => _data;
	private byte** _data = null;

	/// <summary>The number of native strings in the list.</summary>
	public uint Count { get; private set; } = 0;

	// The list capacity
	private uint _capacity = 0;
	#endregion // Fields

	/// <summary>Create an empty list of strings.</summary>
	public NativeStringUTF8List()
	{
		_data = AllocateList(DEFAULT_CAPACITY);
		Count = 0;
		_capacity = DEFAULT_CAPACITY;
	}
	/// <summary>Create a list of native strings from a collection of managed strings.</summary>
	public NativeStringUTF8List(ICollection<string> strings)
	{
		_capacity = Math.Max((uint)strings.Count, DEFAULT_CAPACITY);
		_data = AllocateList(_capacity);
		Count = 0;
		foreach (var str in strings) {
			Add(str);
		}
	}
	/// <summary>Create a list from a params set of strings.</summary>
	public NativeStringUTF8List(params string[] strings)
		: this(strings as ICollection<string>) 
	{ }
	~NativeStringUTF8List()
	{
		dispose();
	}

	// Allocates a new native string from the given string, and adds it to the end of the list.
	public void Add(string str)
	{
		// Resize if needed
		if (Count == _capacity) {
			reserve((uint)(_capacity * 1.5)); // Works as long as we dont start with capacity <= 1
		}
		_data[Count++] = NativeStringUTF8.CreateNativeString(str, out _);
	}

	// Reserves the given list size
	private void reserve(uint newCap)
	{
		var newArr = AllocateList(newCap);
		if (Count > 0) {
			var copySize = Count * sizeof(byte*);
			Buffer.MemoryCopy(_data, newArr, copySize, copySize);
		}
		if (_data != null) {
			Marshal.FreeHGlobal(new IntPtr(_data));
		}
		_data = newArr;
		_capacity = newCap;
	}

	#region IDisposable
	public void Dispose()
	{
		dispose();
		GC.SuppressFinalize(this);
	}

	private void dispose()
	{
		if (_data != null) {
			for (uint si = 0; si < Count; ++si) {
				Marshal.FreeHGlobal(new IntPtr(_data[si]));
			}
			Marshal.FreeHGlobal(new IntPtr(_data));
		}
		_data = null;
	}
	#endregion // IDisposable

	// Allocate a list of string pointers
	private static byte** AllocateList(uint listSize) => 
		(byte**)Marshal.AllocHGlobal((int)(listSize * sizeof(byte*)));
}
