﻿/*
 * MIT License (MIT) - Copyright (c) 2022 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Text;

namespace Vulkan;


#if VVK_API_INTERNAL
internal
#else
public
#endif
static unsafe partial class API
{
	private static partial void LoadGlobals();
	internal static partial void LoadInstance(VkInstance inst);
	internal static partial void LoadDevice(VkDevice device);

	private static void* GetInstanceProcAddr(VkInstance inst, string name)
	{
		var bytes = stackalloc byte[name.Length * 4]; // *4 is worst-case
		var count = Encoding.UTF8.GetBytes(name, new Span<byte>(bytes, name.Length * 4));
		bytes[count] = 0;
		return _vkGetInstanceProcAddr(inst, bytes);
	}

	private static void* GetDeviceProcAddr(VkDevice device, string name)
	{
		var bytes = stackalloc byte[name.Length * 4]; // *4 is worst case
		var count = Encoding.UTF8.GetBytes(name, new Span<byte>(bytes, name.Length * 4));
		bytes[count] = 0;
		return _vkGetDeviceProcAddr(device, bytes);
	}
}
