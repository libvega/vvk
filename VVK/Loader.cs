﻿/*
 * MIT License (MIT) - Copyright (c) 2022 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Vulkan;


/// <summary>
/// Manages the Vulkan loader library and loading operations.
/// </summary>
#if VVK_API_INTERNAL
internal
#else
public
#endif
static class Loader
{
	#region Fields
	/// <summary>The handle to the Vulkan dynamic loader library.</summary>
	public static readonly IntPtr LibraryHandle;

	/// <summary>The file name of the Vulkan dynamic loader library.</summary>
	public static readonly string LibraryName;

	/// <summary>The instance extensions that the API functions have been loaded for.</summary>
	public static InstanceExtensionSet InstanceExtensions { get; private set; } = new();

	/// <summary>The instance object that the API functions have been loaded for.</summary>
	public static API.VkInstance Instance { get; private set; } = new();

	/// <summary>The API version that the API functions have been loaded for.</summary>
	public static uint APIVersion { get; private set; } = 0;

	/// <summary>The device extensions that the API functions have been loaded for.</summary>
	public static DeviceExtensionSet DeviceExtensions { get; private set; } = new();

	/// <summary>The device object that the API functions have been loaded for.</summary>
	public static API.VkDevice Device { get; private set; } = new();
	#endregion // Fields

	/// <summary>
	/// Loads the symbol address with the given name from the loader library, throwing an exception on error.
	/// </summary>
	/// <param name="name">The name of the symbol to load.</param>
	/// <returns>The address of the library symbol.</returns>
	/// <exception cref="ArgumentException">The symbol could not be loaded.</exception>
	public static IntPtr GetExport(string name)
	{
		if (NativeLibrary.TryGetExport(LibraryHandle, name, out var addr)) {
			return addr;
		}
		else {
			throw new ArgumentException($"Could not load symbol '{name}'", nameof(name));
		}
	}

	/// <summary>
	/// Loads the symbol address with the given name from the loader library, returning of the load was successful.
	/// </summary>
	/// <param name="name">The name of the symbol to load.</param>
	/// <param name="address">The loaded address of the symbol.</param>
	/// <returns>If the symbol could be loaded.</returns>
	public static bool TryGetExport(string name, out IntPtr address) => 
		NativeLibrary.TryGetExport(LibraryHandle, name, out address);

	static Loader()
	{
		// Enumerator for searching for Vulkan library paths
		static IEnumerable<string> EnumerateLibraryCandidateNames()
		{
			if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
				yield return "vulkan-1.dll";
			}
			else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) {
				yield return "libMoltenVK.dylib";
			}
			else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
				yield return "libvulkan.so.1";
				yield return "libvulkan.so";
			}
		}

		// Try to load the library
		foreach (var libName in EnumerateLibraryCandidateNames()) {
			if (NativeLibrary.TryLoad(libName, out var handle)) {
				LibraryHandle = handle;
				LibraryName = libName;
				return;
			}
		}

		// Report unsupported platform
		throw new PlatformNotSupportedException(
			"The Vulkan loader library could not be found or loaded on the current system"
		);
	}

	/// <summary>
	/// Loads the instance-level function pointers for the given VkInstance object.
	/// </summary>
	/// <param name="instance">The VkInstance object to load functions for.</param>
	/// <param name="apiVersion">The encoded API version that the instance was created with.</param>
	/// <param name="exts">The instance extensions that are active for the given instance.</param>
	public static void LoadInstanceFunctions(API.VkInstance instance, uint apiVersion, InstanceExtensionSet exts)
	{
		InstanceExtensions = exts;
		Instance = instance;
		APIVersion = apiVersion;
		API.LoadInstance(instance);
	}

	/// <summary>
	/// Loads the device-level function pointers for the given VkDevice object.
	/// </summary>
	/// <param name="device">The VkDevice object to load functions for.</param>
	/// <param name="exts">The device extensions that are active for the given device.</param>
	public static void LoadDeviceFunctions(API.VkDevice device, DeviceExtensionSet exts)
	{
		DeviceExtensions = exts;
		Device = device;
		API.LoadDevice(device);
	}
}
