﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;

namespace VVK;


// Represents an API vendor, and all of the types/commands/extensions associated with the vendor
public class VkVendor
{
	#region Fields
	// The name of the vendor
	public readonly string Name;
	// If this vendor represents the core spec functionality
	public bool IsCore => Name == "Core";

	// The vendor enums
	public readonly Dictionary<string, VkEnum> Enums = new();

	// The vendor types
	public readonly Dictionary<string, VkStruct> Structs = new();

	// The vendor handle types
	public readonly Dictionary<string, VkHandle> Handles = new();

	// The vendor commands
	public readonly Dictionary<string, VkCommand> Commands = new();
	#endregion // Fields

	public VkVendor(string name)
	{
		Name = name;
	}
}
