﻿/*
 * MIT License (MIT) - Copyright (c) 2022 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Text;

namespace VVK;

// Processed spec handle type
public class VkHandle
{
	#region Fields
	// The vendor for the handle type
	public readonly VkVendor Vendor;
	// The name of the handle type
	public readonly string Name;

	// The parent handle type
	public VkHandle? Parent = null;
	#endregion // Fields

	public VkHandle(VkVendor vendor, string name)
	{
		Vendor = vendor;
		Name = name;
	}

	public VkCommand.Level GetCommandLevel()
	{
		// Traverse the parent chain to find the top-level objects that tell us the object level
		VkHandle? curr = this;
		while (curr is not null) {
			if (curr.Name == "VkDevice") {
				return VkCommand.Level.Device;
			}
			else if (curr.Name == "VkInstance") {
				return VkCommand.Level.Instance;
			}
			curr = curr.Parent;
		}
		return VkCommand.Level.Global;
	}

	public void Generate(StringBuilder buffer)
	{
		// Open handle
		buffer.AppendLine("[StructLayout(LayoutKind.Explicit, Size=8)]");
		buffer.Append("public unsafe readonly struct ");
		buffer.Append(Name);
		buffer.Append(" : IVulkanHandle, IEquatable<");
		buffer.Append(Name);
		buffer.AppendLine(">");
		buffer.AppendLine("{");

		// Object type
		buffer.Append("\tpublic static VkObjectType ObjectType => VkObjectType.");
		buffer.Append(Name[2..]);
		buffer.AppendLine(";");
		buffer.AppendLine();

		// Handle field
		buffer.AppendLine("\t[FieldOffset(0)] public readonly nuint Handle;");
		buffer.AppendLine("\tpublic readonly void* RawHandle => (void*)Handle;");
		buffer.AppendLine();

		// Ctor
		buffer.Append("\tpublic ");
		buffer.Append(Name);
		buffer.AppendLine("() => Handle = 0;");
		buffer.Append("\tpublic ");
		buffer.Append(Name);
		buffer.AppendLine("(void* raw) => Handle = (nuint)raw;");
		buffer.Append("\tpublic ");
		buffer.Append(Name);
		buffer.AppendLine("(nuint raw) => Handle = raw;");
		buffer.AppendLine();

		// Equality
		buffer.Append("\tpublic static bool operator == (");
		buffer.Append(Name);
		buffer.Append(" l, ");
		buffer.Append(Name);
		buffer.AppendLine(" r) => l.Handle == r.Handle;");
		buffer.Append("\tpublic static bool operator != (");
		buffer.Append(Name);
		buffer.Append(" l, ");
		buffer.Append(Name);
		buffer.AppendLine(" r) => l.Handle != r.Handle;");
		buffer.Append("\tpublic readonly override bool Equals(object? o) => (o is ");
		buffer.Append(Name);
		buffer.AppendLine(" obj) && (obj.Handle == Handle);");

		// Overrides
		buffer.AppendLine("\tpublic readonly override int GetHashCode() => ((ulong)Handle).GetHashCode();");
		buffer.Append("\tpublic readonly override string ToString() => $\"");
		buffer.Append(Name);
		buffer.AppendLine("{{0x{Handle:X16}}}\";");
		buffer.Append("\tpublic readonly bool Equals(");
		buffer.Append(Name);
		buffer.AppendLine(" o) => o.Handle == Handle;");

		// Bool cast
		buffer.AppendLine();
		buffer.Append("\tpublic static implicit operator bool (");
		buffer.Append(Name);
		buffer.AppendLine(" h) => h.Handle != 0;");

		// Close handle
		buffer.AppendLine("}");
		buffer.AppendLine();
	}
}
