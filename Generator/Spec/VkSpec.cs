﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace VVK;


// Processed spec, a collection of vendors and their associated spec objects
public partial class VkSpec
{
	// Extension type
	public record Extension(string VarName, string ExtName, string? Vendor, bool IsDevice);

	#region Fields
	// API constants
	public readonly Dictionary<string, VkConstant> Constants = new();

	// Extensions
	public readonly Dictionary<string, Extension> Extensions = new();

	// Vendors
	public readonly Dictionary<string, VkVendor> Vendors = new();
	#endregion // Fields

	private VkSpec()
	{

	}

	public VkVendor GetTypeVendor(string typeName)
	{
		if (Char.IsLower(typeName[^1])) {
			return Vendors["Core"];
		}
		else {
			var vendorLen = typeName.AsEnumerable().Reverse().TakeWhile(ch => Char.IsUpper(ch)).Count();
			var vendor = typeName.Substring(typeName.Length - vendorLen);
			return Vendors.TryGetValue(vendor, out var vend) ? vend : Vendors["Core"];
		}
	}

	public static bool TryProcess(RawSpec rawSpec, [NotNullWhen(true)] out VkSpec? spec)
	{
		// Create the spec and vendors
		spec = new();
		spec.Vendors.Add("Core", new("Core")); // The "Core" vendor is for the core vulkan spec objects
		foreach (var vendor in rawSpec.Vendors) {
			spec.Vendors.Add(vendor!, new(vendor));
		}

		// Process the api constants
		if (Args.Verbose) {
			Info("Processing constants");
		}
		foreach (var rawConst in rawSpec.Constants) {
			if (!VkConstant.TryProcess(spec, rawConst.Value, out var vkConst)) {
				return false;
			}
			spec.Constants.Add(vkConst.Name, vkConst);
			if (Args.Debug) {
				Debug($"  Processed constant '{vkConst.Name}' : {vkConst.Type} = {vkConst.ValueString}");
			}
		}

		// Process the extensions
		if (Args.Verbose) {
			Info("Processing extensions");
		}
		foreach (var ext in rawSpec.Extensions.Values) {
			var varName = ext.Name.ToUpperInvariant() + "_EXTENSION_NAME";
			string? vendorName = null;
			var split = varName.Split('_', StringSplitOptions.RemoveEmptyEntries);
			if (spec.Vendors.TryGetValue(split[1], out var vendor)) {
				vendorName = vendor.Name;
			}
			spec.Extensions.Add(ext.Name, new(varName, ext.Name, vendorName, ext.IsDevice));
		}

		// Process the enums into the vendors
		if (!TryProcessEnums(rawSpec, spec)) {
			return false;
		}

		// Process the handle types
		if (!TryProcessHandles(rawSpec, spec)) {
			return false;
		}

		// Process the structs into the vendors
		if (!TryProcessStructs(rawSpec, spec)) {
			return false;
		}

		// Process the commands
		if (!TryProcessCommands(rawSpec, spec)) {
			return false;
		}

		// If a vendor list is specified, apply it
		if (Args.VendorList is not null) {
			List<string> vendorRemove = new();
			List<string> extRemove = new();
			if (Args.VendorSkip) {
				foreach (var vendor in Args.VendorList) {
					if (vendor == "Core") {
						continue; // Never remove Core vendor
					}
					if (spec.Vendors.ContainsKey(vendor)) {
						vendorRemove.Add(vendor);
					}
					else {
						Warning($"Could not find vendor '{vendor}' to skip generation");
					}
					extRemove.AddRange(spec.Extensions.Where(p => p.Value.Vendor == vendor).Select(p => p.Key));
				}
			}
			else {
				foreach (var vendor in Args.VendorList) {
					if (!spec.Vendors.ContainsKey(vendor)) {
						Warning($"Could not find vendor '{vendor}' to generate");
					}
				}
				foreach (var vendor in spec.Vendors) {
					if (vendor.Key == "Core") {
						continue; // Never remove Core vendor
					}
					if (!Args.VendorList.Contains(vendor.Key)) {
						vendorRemove.Add(vendor.Key);
						extRemove.AddRange(spec.Extensions.Where(p => p.Value.Vendor == vendor.Key).Select(p => p.Key));
					}
				}
			}
			foreach (var rem in vendorRemove) {
				spec.Vendors.Remove(rem);
			}
			foreach (var rem in extRemove) {
				spec.Extensions.Remove(rem);
			}
		}

		return true;
	}

	private static bool TryProcessEnums(RawSpec rawSpec, VkSpec vkSpec)
	{
		if (Args.Verbose) {
			Info("Processing enums");
		}

		foreach ((var rawEnumName, var rawEnum) in rawSpec.Enums) {
			// Skip processing for duplicate "Flags"/"FlagBits" enums
			if ((rawEnumName != rawEnum.Name) && rawEnumName.Contains("FlagBits")) {
				continue;
			}

			// Get the enum vendor
			var vendor = vkSpec.GetTypeVendor(rawEnumName);

			// Create and add the enum
			VkEnum @enum = new(vendor, rawEnumName, rawEnum.IsFlags, rawEnum.Size);
			vendor.Enums.Add(@enum.Name, @enum);
			if (Args.Debug) {
				Debug($"  Processing enum '{rawEnumName}' (base name {@enum.BaseName})");
			}

			// Sort the enum values by alias reference
			rawEnum.Values.Sort((l, r) => {
				if (l.Alias is not null) {
					return (r.Alias is not null) ? 0 : 1;
				}
				else {
					return (r.Alias is null) ? 0 : -1;
				}
			});

			// Process the enum values
			foreach (var value in rawEnum.Values) {
				if (!@enum.TryAddValue(vkSpec, value)) {
					return false;
				}
			}
		}

		return true;
	}

	private static bool TryProcessHandles(RawSpec rawSpec, VkSpec vkSpec)
	{
		if (Args.Verbose) {
			Info("Processing handles");
		}

		// Simply translate the structs into their vendors
		foreach ((var rawHandleName, var rawHandle) in rawSpec.Handles) {
			var vendor = vkSpec.GetTypeVendor(rawHandleName);
			VkHandle handle = new(vendor, rawHandleName);
			vendor.Handles.Add(rawHandleName, handle);
		}

		// Try to find the parents for the handles
		foreach (var handle in vkSpec.Vendors.Values.SelectMany(v => v.Handles)) {
			var rawHandle = rawSpec.Handles[handle.Key];
			if (rawHandle.Parent is null) {
				if (Args.Debug) {
					Debug($"  Processed handle '{handle.Key}' (parent = null)");
				}
				continue;
			}
			var parentVendor = vkSpec.GetTypeVendor(rawHandle.Parent);
			if (!parentVendor.Handles.TryGetValue(rawHandle.Parent, out var parentHandle)) {
				Error($"Could not find parent type '{rawHandle.Parent}' for handle type '{handle.Key}'");
				return false;
			}
			handle.Value.Parent = parentHandle;
			if (Args.Debug) {
				Debug($"  Processed handle '{handle.Key}' (parent = {parentHandle.Name})");
			}
		}
		return true;
	}

	private static bool TryProcessStructs(RawSpec rawSpec, VkSpec vkSpec)
	{
		if (Args.Verbose) {
			Info("Processing structs");
		}

		// First setup the structs without performing full processing
		foreach ((var rawStructName, var rawStruct) in rawSpec.Structs) {
			var vendor = vkSpec.GetTypeVendor(rawStructName);
			VkStruct vkStruct = new(vendor, rawStructName, rawStruct.IsUnion, rawStruct.StructureType, rawStruct.Fields);
			vendor.Structs.Add(rawStructName, vkStruct);
		}

		if (Args.Verbose) {
			Info("Processing struct fields");
		}

		// Go through all types and calculate their final sizes
		foreach (var vendor in vkSpec.Vendors.Values) {
			foreach (var @struct in vendor.Structs.Values) {
				if (!@struct.ProcessFields(vkSpec)) {
					return false;
				}
				if (Args.Debug) {
					Debug($"  Processed struct '{@struct.Name}' (vendor {@struct.Vendor.Name}) (size {@struct.Size})");
				}
			}
		}

		return true;
	}

	private static bool TryProcessCommands(RawSpec rawSpec, VkSpec vkSpec)
	{
		if (Args.Verbose) {
			Info("Processing commands");
		}

		// Process the commands
		foreach ((var cmdName, var rawCommand) in rawSpec.Commands) {
			if (!VkCommand.TryProcess(vkSpec, rawCommand, out var vkCmd)) {
				return false;
			}
			vkCmd.Vendor.Commands.Add(vkCmd.Name, vkCmd);
			if (Args.Debug) {
				Debug($"  Processed command '{vkCmd.Name}' (vendor {vkCmd.Vendor.Name}, level {vkCmd.ObjectLevel})");
			}
		}

		return true;
	}
}
