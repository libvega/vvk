﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace VVK;


// Processed API command
// Unlike other processed types and the raw commands, these are not shared objects between different vendors when
//   aliased, since commands have special loading logic that must be applied to them.
public class VkCommand
{
	// Parameter
	public record Param(string Name, string Type);
	// Level of command
	public enum Level { Global, Instance, Device };

	#region Fields
	// The vendor for the command
	public readonly VkVendor Vendor;
	// The name of the command, with "vk" still on the front
	public readonly string Name;
	// The type name of the command return type
	public readonly string ReturnType;
	// The version or extension required for the command
	public readonly string Feature;
	// The object level namespace of the command
	public readonly Level ObjectLevel;
	// If the command is aliased, this is the target command
	public readonly string? Alias;

	// The in-order parameters
	public readonly List<Param> Params = new();
	#endregion // Fields

	private VkCommand(VkVendor vendor, string name, string returnType, string feature, Level level, string? alias)
	{
		Vendor = vendor;
		Name = name;
		ReturnType = returnType;
		Feature = feature;
		ObjectLevel = level;
		Alias = alias;
	}

	public static bool TryProcess(VkSpec spec, RawCommand rawCmd, [NotNullWhen(true)] out VkCommand? vkCmd)
	{
		vkCmd = null;

		// Process the return type
		string retTypeName;
		{
			var retType = spec.GetType(rawCmd.ReturnType);
			if (retType is VkSpec.BuiltinType biType) {
				retTypeName = biType.GenName;
			}
			else if (retType is VkStruct vkStruct) {
				retTypeName = vkStruct.Name;
			}
			else if (retType is VkEnum vkEnum) {
				retTypeName = vkEnum.Name;
			}
			else if (retType is VkHandle vkHandle) {
				retTypeName = vkHandle.Name;
			}
			else if (retType is VkSpec.FunctionPointer) {
				retTypeName = "void*";
			}
			else {
				Error($"Unknown return type '{rawCmd.ReturnType}' for command '{rawCmd.Name}'");
				return false;
			}
		}

		// Check for object type using the first argument
		Level objLevel;
		if (LEVEL_OVERRIDES.TryGetValue(rawCmd.Name, out var levelOverride)) {
			objLevel = levelOverride;
		}
		else {
			var levelType = spec.GetType(rawCmd.Params[0].Type);
			if (levelType is VkHandle vkHandle) {
				objLevel = vkHandle.GetCommandLevel();
			}
			else if (levelType is null) {
				Error($"Cannot detect command level from parameter 0 ('{rawCmd.Params[0].Type}') for command '{rawCmd.Name}'");
				return false;
			}
			else {
				objLevel = Level.Global; // Assume that all non-handle functions are at the global level
			}
		}

		// Create the command without args
		vkCmd = new(spec.GetTypeVendor(rawCmd.Name), rawCmd.Name, retTypeName, rawCmd.Requires, objLevel, rawCmd.Alias);

		// Process params
		foreach (var @param in rawCmd.Params) {
			var paramType = spec.GetType(@param.Type);
			string paramTypeName;
			if (paramType is VkSpec.BuiltinType biType) {
				if (biType.GenName == "void" && @param.PtrCount == 0) {
					Error($"Cannot handle opaque type '{biType.SpecName}' without a pointer in command {rawCmd.Name}");
					return false;
				}
				paramTypeName = biType.GenName;
			}
			else if (paramType is VkStruct vkStruct) {
				paramTypeName = vkStruct.Name;
			}
			else if (paramType is VkEnum vkEnum) {
				paramTypeName = vkEnum.Name;
			}
			else if (paramType is VkHandle vkHandle) {
				paramTypeName = vkHandle.Name;
			}
			else if (paramType is VkSpec.FunctionPointer) {
				paramTypeName = "void*";
			}
			else {
				Error($"Unknown param type '{@param.Type}' for command '{rawCmd.Name}' for parameter '{@param.Name}'");
				return false;
			}
			if (@param.PtrCount > 0) {
				paramTypeName += new string('*', (int)@param.PtrCount);
			}

			string paramName = @param.Name;
			if (paramName == "object" || paramName == "event") {
				paramName = '@' + paramName;
			}

			vkCmd.Params.Add(new(paramName, paramTypeName));
		}

		return true;
	}

	public void GenerateField(StringBuilder buffer)
	{
		buffer.Append("\tprivate static delegate* unmanaged<");
		foreach (var par in Params) {
			buffer.Append(par.Type);
			buffer.Append(", ");
		}
		buffer.Append(ReturnType);
		buffer.Append("> _");
		buffer.Append(Name);
		buffer.AppendLine(" = null;");
	}

	public void GenerateLoad(VkSpec spec, StringBuilder buffer)
	{
		// Check for an alias
		bool hasAlias = (Alias is not null);
		if (hasAlias) {
			buffer.Append("\t\tif (_");
			buffer.Append(Alias);
			buffer.AppendLine(" != null) {");
			buffer.Append("\t\t\t_");
			buffer.Append(Name);
			buffer.Append(" = _");
			buffer.Append(Alias);
			buffer.AppendLine(";");
			buffer.AppendLine("\t\t}");
		}

		// Potential feature guard
		var hasGuard = (ObjectLevel != Level.Global) && (Feature != "1_0");
		if (hasGuard) {
			if (Char.IsDigit(Feature[0])) { // Version requirement
				buffer.Append("\t\t");
				if (hasAlias) {
					buffer.Append("else ");
				}
				buffer.Append("if (Loader.APIVersion >= VK_API_VERSION_");
				buffer.Append(Feature);
				buffer.AppendLine(") {");
			}
			else { // Extension requirement
				var ext = spec.Extensions[Feature];
				buffer.Append("\t\t");
				if (hasAlias) {
					buffer.Append("else ");
				}
				if (ext.IsDevice) {
					buffer.Append("if (Loader.DeviceExtensions.");
				}
				else {
					buffer.Append("if (Loader.InstanceExtensions.");
				}
				buffer.Append(ext.ExtName);
				buffer.AppendLine(") {");
			}
		}

		// Assignment
		buffer.Append(hasGuard ? "\t\t\t_" : "\t\t_");
		buffer.Append(Name);
		buffer.AppendLine(" =");
		buffer.Append(hasGuard ? "\t\t\t\t(delegate* unmanaged<" : "\t\t\t(delegate* unmanaged<");
		foreach (var @param in Params) {
			buffer.Append(@param.Type);
			buffer.Append(", ");
		}
		buffer.Append(ReturnType);
		buffer.AppendLine(">)");
		if (ObjectLevel == Level.Global) {
			buffer.Append(hasGuard ? "\t\t\t\tLoader.GetExport(\"" : "\t\t\tLoader.GetExport(\"");
			buffer.Append(Name);
			buffer.AppendLine("\").ToPointer();");
		}
		else if (ObjectLevel == Level.Instance) {
			buffer.Append(hasGuard ? "\t\t\t\tGetInstanceProcAddr(inst, \"" : "\t\t\tGetInstanceProcAddr(inst, \"");
			buffer.Append(Name);
			buffer.AppendLine("\");");
		}
		else {
			buffer.Append(hasGuard ? "\t\t\t\tGetDeviceProcAddr(device, \"" : "\t\t\tGetDeviceProcAddr(device, \"");
			buffer.Append(Name);
			buffer.AppendLine("\");");
		}

		// Close guard
		if (hasGuard) {
			buffer.AppendLine("\t\t}");
		}
	}

	public void GenerateCall(StringBuilder buffer)
	{
		buffer.Append("\tpublic static ");
		buffer.Append(ReturnType);
		buffer.Append(' ');
		buffer.Append(Name);
		buffer.Append('(');
		foreach (var par in Params) {
			buffer.Append(par.Type);
			buffer.Append(' ');
			buffer.Append(par.Name);
			buffer.Append(", ");
		}
		buffer.Remove(buffer.Length - 2, 2);
		buffer.AppendLine(") =>");

		buffer.Append("\t\t_");
		buffer.Append(Name);
		buffer.Append('(');
		foreach (var par in Params) {
			buffer.Append(par.Name);
			buffer.Append(", ");
		}
		buffer.Remove(buffer.Length - 2, 2);
		buffer.AppendLine(");");
	}

	// Special overrides for command levels
	private static readonly Dictionary<string, Level> LEVEL_OVERRIDES = new() {
		{ "vkGetInstanceProcAddr", Level.Global },
		{ "vkGetDeviceProcAddr", Level.Global }
	};
}
