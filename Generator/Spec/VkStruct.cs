﻿/*
 * MIT License (MIT) - Copyright (c) 2022 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VVK;


// Processed struct type
public class VkStruct
{
	// Processed struct field
	public record Field(uint Index, string Name, string Type, uint? ArrSize, uint PtrDepth, uint Offset, bool Fixed,
		bool IsStruct);
	// Special type for fixed arrays of types that cannot normally be fixed
	public record ArrayType(string Name, string Type, uint TypeSize, uint FullSize);

	#region Fields
	// The vendor for the struct type
	public readonly VkVendor Vendor;
	// The struct type name
	public readonly string Name;
	// If the struct is actually a union
	public readonly bool IsUnion;

	// Special case where the struct should not be generated (used because ANDROID decided to put internal types in the spec)
	public bool Disabled = false;

	// The processed structure type enum value name, if present
	public string? StructureType = null;

	// The size of the struct, calculated after all struct types are loaded
	public uint Size = 0;
	// The alignment requirements of the struct
	public uint Alignment = 0;

	// The fields (order is preserved using the Index field)
	public readonly Dictionary<string, Field> Fields = new();

	// Special fixed array types
	public readonly Dictionary<string, ArrayType> ArrayTypes = new();

	// Reference to the raw fields for delayed processing
	private readonly List<RawStruct.Field> _rawFields;

	// Information about the field types
	public bool HasFixedField { get; private set; } = false;
	public bool HasPointerField { get; private set; } = false;
	public bool HasArrayField { get; private set; } = false;
	#endregion // Fields

	public VkStruct(VkVendor vendor, string name, bool union, string? structType, List<RawStruct.Field> rawFields)
	{
		Vendor = vendor;
		Name = name;
		IsUnion = union;
		StructureType = structType;
		_rawFields = rawFields;
	}

	public bool ProcessFields(VkSpec spec)
	{
		// Check if size is already calculated
		if (Size != 0) {
			return true;
		}

		// Process structure type
		if (StructureType is not null) {
			var sTypeEnum = spec.Vendors["Core"].Enums["VkStructureType"];
			StructureType = spec.SanitizeEnumValueName(StructureType, sTypeEnum);
			if (!sTypeEnum.Values.TryGetValue(StructureType, out _)) {
				Disabled = true;
				return true;
			}
		}

		// Iterate over the raw fields
		foreach (var rawField in _rawFields) {
			// Lookup the type
			string typeName;
			uint typeSize, alignment;
			bool canFix;
			var typeObj = spec.GetType(rawField.Type);
			var isStruct = false;
			if (typeObj is VkSpec.BuiltinType biInfo) {
				typeName = biInfo.GenName;
				typeSize = biInfo.Size;
				alignment = Math.Min(typeSize, 8);
				canFix = biInfo.CanFix;
				// Extra check necessary for the platform-specific types
				if (typeName == "void") {
					if (rawField is not RawStruct.PointerField) {
						Error($"Cannot handle non-pointer platform type '{rawField.Type}'");
						return false;
					}
				}
			}
			else if (typeObj is VkStruct fStruct) {
				if (!fStruct.ProcessFields(spec)) {
					return false;
				}
				typeName = fStruct.Name;
				typeSize = fStruct.Size;
				alignment = fStruct.Alignment;
				canFix = false;
				isStruct = true;
				HasPointerField = HasPointerField || fStruct.HasPointerField;
				HasFixedField = HasFixedField || fStruct.HasFixedField;
				HasArrayField = HasArrayField || fStruct.HasArrayField;
			}
			else if (typeObj is VkEnum fEnum) {
				typeName = fEnum.Name;
				typeSize = fEnum.Size / 8u;
				alignment = typeSize;
				canFix = false;
			}
			else if (typeObj is VkHandle fHandle) {
				typeName = fHandle.Name;
				typeSize = 8;
				alignment = 8;
				canFix = false;
				HasPointerField = true;
			}
			else if (typeObj is VkSpec.FunctionPointer) {
				typeName = "void*";
				typeSize = 8;
				alignment = 8;
				canFix = false;
				HasPointerField = true;
			}
			else {
				Error($"Cound not find type '{rawField.Type}' for field {Name}::{rawField.Name}");
				return false;
			}

			// Align the current size if needed
			if (rawField is RawStruct.PointerField) {
				typeSize = 8; // All pointers are size 8
				alignment = 8;
			}
			if ((Size % alignment) != 0) {
				Size += alignment - (Size % alignment);
			}
			if (alignment > Alignment) {
				Alignment = Math.Min(alignment, 8);
			}
			var currentOffset = IsUnion ? 0u : Size;

			// Switch on the field type to add the fields
			var fieldName = Char.ToUpperInvariant(rawField.Name[0]) + rawField.Name.Substring(1);
			if (rawField is RawStruct.ArrayField arrField) {
				// Parse the array size
				uint arrSize = 1;
				var sizeParts = arrField.ArraySize.Split('*', StringSplitOptions.RemoveEmptyEntries);
				foreach (var part in sizeParts) {
					if (UInt32.TryParse(part, out var partSize)) {
						arrSize *= partSize;
					}
					else if (part.StartsWith("VK_")) {
						if (!spec.Constants.TryGetValue(part, out var constant)) {
							Error($"Could not find constant value '{part}' for array size");
							return false;
						}
						if (!UInt32.TryParse(constant.ValueString, out var constSize)) {
							Error($"Invalid value for array size constant '{part}' = {constant.ValueString}");
							return false;
						}
						arrSize *= constSize;
					}
					else {
						Error($"Unparsable array size part '{part}' (in '{arrField.ArraySize}')");
						return false;
					}
				}

				// Create a fixed array if possible, otherwise create a new array type
				if (canFix) {
					Fields.Add(fieldName,
						new((uint)Fields.Count, fieldName, typeName, arrSize, 0, currentOffset, true, isStruct));
					HasFixedField = true;
				}
				else {
					var arrTypeName = $"{typeName}Array{arrSize}";
					ArrayTypes.TryAdd(arrTypeName, new(arrTypeName, typeName, typeSize, typeSize * arrSize));
					Fields.Add(fieldName,
						new((uint)Fields.Count, fieldName, arrTypeName, null, 0, currentOffset, false, isStruct));
					HasArrayField = true;
				}

				typeSize *= arrSize; // Set the type size to the whole array size
			}
			else if (rawField is RawStruct.PointerField ptrField) {
				Fields.Add(fieldName,
					new((uint)Fields.Count, fieldName, typeName, null, ptrField.PtrCount, currentOffset, false, isStruct));
				HasPointerField = true;
			}
			else {
				Fields.Add(fieldName,
					new((uint)Fields.Count, fieldName, typeName, null, 0, currentOffset, false, isStruct));
			}

			// Increase type size
			if (IsUnion) {
				Size = Math.Max(Size, typeSize);
			}
			else {
				Size += typeSize;
			}
		}

		// Final alignment check
		if ((Size % Alignment) != 0) {
			Size += Alignment - (Size % Alignment);
		}

		return true;
	}

	public void Generate(StringBuilder buffer)
	{
		// Dont generate disabled types
		if (Disabled) {
			return;
		}

		var maxFieldIndex = Fields.Values.Max(f => f.Index);
		var emptyType = !Fields.Any() || 
			((Fields.Count == 2) && Fields.ContainsKey("SType") && Fields.ContainsKey("PNext"));

		// Open struct
		buffer.Append("[StructLayout(LayoutKind.Explicit, Size=");
		buffer.Append(Size);
		buffer.AppendLine(")]");
		buffer.Append("public unsafe struct ");
		buffer.Append(Name);
		buffer.Append(" : IEquatable<");
		buffer.Append(Name);
		buffer.AppendLine(">");
		buffer.AppendLine("{");

		// Write the fields
		foreach (var field in Fields.Values.OrderBy(f => f.Index)) {
			bool isSType = (field.Name == "SType") && (field.Type == "VkStructureType") && (StructureType is not null);

			buffer.Append("\t[FieldOffset(");
			buffer.Append(field.Offset);
			buffer.Append(")] public ");
			if (field.Fixed) {
				buffer.Append("fixed ");
			}
			else if (isSType) {
				buffer.Append("readonly ");
			}
			buffer.Append(field.Type);
			for (uint pi = 0; pi < field.PtrDepth; ++pi) {
				buffer.Append('*');
			}
			buffer.Append(' ');
			buffer.Append(field.Name);
			if (field.Fixed) {
				buffer.Append('[');
				buffer.Append(field.ArrSize);
				buffer.AppendLine("];");
			}
			else if (isSType) {
				buffer.Append(" = VkStructureType.");
				buffer.Append(StructureType);
				buffer.AppendLine(";");
			}
			else {
				buffer.AppendLine(" = default;");
			}
		}

		// Default constructor
		buffer.AppendLine();
		buffer.Append("\tpublic ");
		buffer.Append(Name);
		if (StructureType is not null) {
			buffer.AppendLine("()");
			buffer.AppendLine("\t{");
			buffer.Append("\t\tSType = VkStructureType.");
			buffer.Append(StructureType);
			buffer.AppendLine(";");
			buffer.AppendLine("\t}");
		}
		else {
			buffer.AppendLine("() { }");
		}

		// Constructor with arguments
		//   Only generated for types without fixed or array fields, since those are hard to pass as arguments
		//   Pointers are fine since they are easy to pass as arguments
		//   We skip SType, since that is a set value, and PNext, since that has a special meaning
		if (!HasFixedField && !HasArrayField && !emptyType) {
			buffer.AppendLine();
			buffer.Append("\tpublic ");
			buffer.Append(Name);
			buffer.AppendLine("(");
			foreach (var field in Fields.Values.OrderBy(f => f.Index)) {
				if (field.Name == "SType" || field.Name == "PNext") {
					continue;
				}
				var isLast = field.Index == maxFieldIndex;
				buffer.Append("\t\t");
				if (field.IsStruct) {
					buffer.Append("in ");
				}
				buffer.Append(field.Type);
				for (uint pi = 0; pi < field.PtrDepth; ++pi) {
					buffer.Append('*');
				}
				buffer.Append(' ');
				if (field.Name == "Object") {
					buffer.Append("@");
				}
				buffer.Append(Char.ToLower(field.Name[0]));
				buffer.Append(field.Name.AsSpan(1));
				buffer.Append(" = ");
				if (field.PtrDepth > 0) {
					buffer.Append("null");
				}
				else {
					buffer.Append("default");
				}
				buffer.AppendLine(isLast ? String.Empty : ",");
			}
			buffer.AppendLine("\t) {");
			foreach (var field in Fields.Values.OrderBy(f => f.Index)) {
				if (field.Name == "SType") {
					buffer.Append("\t\tSType = VkStructureType.");
					buffer.Append(StructureType);
					buffer.AppendLine(";");
					continue;
				}
				if (field.Name == "PNext") {
					buffer.AppendLine("\t\tPNext = null;");
					continue;
				}

				buffer.Append("\t\t");
				buffer.Append(field.Name);
				buffer.Append(" = ");
				if (field.Name == "Object") {
					buffer.Append("@");
				}
				buffer.Append(Char.ToLower(field.Name[0]));
				buffer.Append(field.Name.AsSpan(1));
				buffer.AppendLine(";");
			}
			buffer.AppendLine("\t}");
		}

		// ToString
		buffer.AppendLine();
		buffer.AppendLine("\tpublic readonly override string ToString()");
		buffer.AppendLine("\t{");
		buffer.AppendLine("\t\tStringBuilder sb = GetCachedThreadStringBuilder();");
		buffer.Append("\t\tsb.Append(\"");
		buffer.Append(Name);
		buffer.AppendLine("{\");");
		foreach (var field in Fields.Values.OrderBy(f => f.Index)) {
			buffer.Append("\t\tsb.Append(\" ");
			buffer.Append(field.Name);
			buffer.Append("=\"); ");
			if (field.ArrSize > 0) {
				buffer.Append("sb.Append(\"");
				buffer.Append(field.Type);
				buffer.Append("[");
				buffer.Append(field.ArrSize);
				buffer.AppendLine("]\");");
			}
			else if ((field.PtrDepth > 0) || field.Type.StartsWith("void")) {
				buffer.Append("sb.Append(((ulong)");
				buffer.Append(field.Name);
				buffer.AppendLine(").ToString(\"X16\"));");
			}
			else {
				buffer.Append("sb.Append(");
				buffer.Append(field.Name);
				buffer.AppendLine(");");
			}
		}
		buffer.AppendLine("\t\tsb.Append(\" }\");");
		buffer.AppendLine("\t\treturn sb.ToString();");
		buffer.AppendLine("\t}");

		// GetHashCode
		//    Note: only generated for types without fixed or array types, SType or any pointers
		buffer.AppendLine();
		if (HasFixedField || HasArrayField || HasPointerField) {
			// Generate an error obsolete function that throws a not implemented error
			var errorStr = $"{Name} has fields that are incompatible with generating a hash code";
			buffer.Append("\t[Obsolete(\"");
			buffer.Append(errorStr);
			buffer.AppendLine("\", true)]");
			buffer.AppendLine("\tpublic readonly override int GetHashCode() => "); 
			buffer.Append("\t\tthrow new NotImplementedException(\"");
			buffer.Append(errorStr);
			buffer.AppendLine("\");");
		}
		else {
			var canCombine = (Fields.Count <= 8); // HashCode.Combine is better than the streaming API (<=8 only)
			buffer.AppendLine("\tpublic readonly override int GetHashCode()");
			buffer.AppendLine("\t{");
			if (canCombine) {
				buffer.AppendLine("\t\treturn HashCode.Combine(");
			}
			else {
				buffer.AppendLine("\t\tHashCode hc = new();");
			}

			foreach (var field in Fields.Values.OrderBy(f => f.Index)) {
				var isLast = field.Index == maxFieldIndex;
				if (canCombine) {
					buffer.Append("\t\t\t");
					buffer.Append(field.Name);
					buffer.AppendLine(isLast ? String.Empty : ",");
				}
				else {
					buffer.Append("\t\thc.Add(");
					buffer.Append(field.Name);
					buffer.AppendLine(");");
				}
			}

			if (canCombine) {
				buffer.AppendLine("\t\t);");
			}
			else {
				buffer.AppendLine("\t\treturn hc.GetHashCode();");
			}
			buffer.AppendLine("\t}");
		}

		// Equals
		buffer.AppendLine();
		buffer.Append("\tpublic readonly bool Equals(");
		buffer.Append(Name);
		buffer.AppendLine(" o) => Equals(in o);");
		buffer.AppendLine();
		buffer.Append("\tpublic readonly override bool Equals(object? o) => (o is ");
		buffer.Append(Name);
		buffer.AppendLine(" obj) && Equals(in obj);");
		buffer.AppendLine();
		buffer.Append("\tpublic readonly bool Equals(in ");
		buffer.Append(Name);
		buffer.AppendLine(" other)");
		buffer.AppendLine("\t{");
		if (HasFixedField) {
			buffer.Append("\t\tfixed (");
			buffer.Append(Name);
			buffer.AppendLine("* tptr = &this)");
			buffer.Append("\t\tfixed (");
			buffer.Append(Name);
			buffer.AppendLine("* optr = &other)");
		}
		buffer.AppendLine("\t\treturn");
		foreach (var field in Fields.Values.OrderBy(f => f.Index)) {
			var last = field.Index == maxFieldIndex;
			buffer.Append("\t\t\t");
			if (field.Fixed) {
				// Primitive types can just use InternalExtensions.Memcmp
				buffer.Append("Memcmp(tptr->");
				buffer.Append(field.Name);
				buffer.Append(", optr->");
				buffer.Append(field.Name);
				buffer.Append(", ");
				buffer.Append(field.ArrSize);
				buffer.Append("ul)");
			}
			else {
				buffer.Append('(');
				buffer.Append(field.Name);
				buffer.Append(" == other.");
				buffer.Append(field.Name);
				buffer.Append(')');
			}
			buffer.AppendLine(last ? ";" : " &&");
		}
		buffer.AppendLine("\t}");

		// Equality operators
		buffer.AppendLine();
		buffer.Append("\tpublic static bool operator == (in ");
		buffer.Append(Name);
		buffer.Append(" l, in ");
		buffer.Append(Name);
		buffer.AppendLine(" r) => l.Equals(in r);");
		buffer.Append("\tpublic static bool operator != (in ");
		buffer.Append(Name);
		buffer.Append(" l, in ");
		buffer.Append(Name);
		buffer.AppendLine(" r) => !l.Equals(in r);");

		// Write the fixed array types if present
		if (ArrayTypes.Count > 0) {
			buffer.AppendLine();
			buffer.AppendLine("\t#pragma warning disable CS0659, CS0661  // Warnings about missing GetHashCode()");

			foreach (var atype in ArrayTypes.Values) {
				// Open the array type
				buffer.Append("\t[StructLayout(LayoutKind.Explicit, Size=");
				buffer.Append(atype.FullSize);
				buffer.AppendLine(")]");
				buffer.Append("\tpublic unsafe struct ");
				buffer.AppendLine(atype.Name);
				buffer.AppendLine("\t{");

				// Capacity constant
				var acount = atype.FullSize / atype.TypeSize;
				buffer.Append("\t\tpublic const uint Capacity = ");
				buffer.Append(acount);
				buffer.AppendLine(";");

				// Raw buffer field
				buffer.AppendLine();
				buffer.Append("\t\t[FieldOffset(0)] private fixed byte _data[");
				buffer.Append(atype.FullSize);
				buffer.AppendLine("];");

				// Setter/Getter
				buffer.AppendLine();
				buffer.Append("\t\tpublic ref ");
				buffer.Append(atype.Type);
				buffer.AppendLine(" this[int index] {");
				{
					buffer.AppendLine("\t\t\tget { ");
					buffer.AppendLine("\t\t\t\tif (index < 0 || index >= Capacity) throw new ArgumentOutOfRangeException(\"index\");");
					buffer.Append("\t\t\t\treturn ref Unsafe.As<byte, ");
					buffer.Append(atype.Type);
					buffer.Append(">(ref Unsafe.AsRef(in _data[index * sizeof(");
					buffer.Append(atype.Type);
					buffer.AppendLine(")]));");
					buffer.AppendLine("\t\t\t}");
				}
				buffer.AppendLine("\t\t}");

				// Equality function
				buffer.AppendLine();
				buffer.Append("\t\tpublic readonly override bool Equals(object? o) => (o is ");
				buffer.Append(atype.Name);
				buffer.AppendLine(" obj) && Equals(obj);");
				buffer.AppendLine();
				buffer.Append("\t\tpublic readonly bool Equals(in ");
				buffer.Append(atype.Name);
				buffer.AppendLine(" other)");
				buffer.AppendLine("\t\t{");
				buffer.Append("\t\t\tfixed (");
				buffer.Append(atype.Name);
				buffer.AppendLine("* tptr = &this)");
				buffer.Append("\t\t\tfixed (");
				buffer.Append(atype.Name);
				buffer.AppendLine("* optr = &other)");
				buffer.AppendLine("\t\t\treturn Memcmp(tptr, optr, 1);");
				buffer.AppendLine("\t\t}");

				// Equality operators
				buffer.AppendLine();
				buffer.Append("\t\tpublic static bool operator == (in ");
				buffer.Append(atype.Name);
				buffer.Append(" l, in ");
				buffer.Append(atype.Name);
				buffer.AppendLine(" r) => l.Equals(r);");
				buffer.Append("\t\tpublic static bool operator != (in ");
				buffer.Append(atype.Name);
				buffer.Append(" l, in ");
				buffer.Append(atype.Name);
				buffer.AppendLine(" r) => !l.Equals(r);");

				// Close the array type
				buffer.AppendLine("\t}");
			}

			buffer.AppendLine("\t#pragma warning restore CS0659, CS0661");
		}

		// Close struct
		buffer.AppendLine("}");
		buffer.AppendLine();
	}
}
