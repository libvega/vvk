﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Text;

namespace VVK;


// Processed spec enum type and values
public class VkEnum
{
	// Processed enum value
	public record Value(string Name, int IntValue, string? Alias);

	#region Fields
	// The vendor for the enum type
	public readonly VkVendor Vendor;
	// The name of the enum type
	public readonly string Name;
	// The base name of the type, without the vendor suffix, or "Flags"
	public readonly string BaseName;
	// If the enum acts as a bitmask
	public readonly bool IsBitmask;
	// The size of the enum type (32 or 64)
	public readonly uint Size;

	// The values
	public readonly Dictionary<string, Value> Values = new();
	#endregion // Fields

	public VkEnum(VkVendor vendor, string name, bool bitmask, uint size)
	{
		var isVendored = name.EndsWith(vendor.Name);

		Vendor = vendor;
		Name = name;
		BaseName = isVendored ? Name.Substring(0, Name.Length - Vendor.Name.Length) : Name;
		if (bitmask && BaseName.Contains("Flags")) {
			BaseName = BaseName.Replace("Flags", "");
		}
		IsBitmask = bitmask;
		Size = size;
	}

	public bool TryAddValue(VkSpec spec, RawEnum.Value rawValue)
	{
		// Convert to C# field name
		var vname = spec.SanitizeEnumValueName(rawValue.Name, this);

		// Try to add if it matches (SanitizeEnumValueName will strip the enum name if it matches)
		if (!vname.StartsWith(BaseName)) {
			// Special case for "VkResult", which doesn't follow naming rules
			if ((BaseName == "VkResult") && vname.StartsWith("Vk")) {
				vname = vname.Substring(2);
			}

			Value value;
			if (rawValue.Alias is not null) {
				var aname = spec.SanitizeEnumValueName(rawValue.Alias, this);
				if ((BaseName == "VkResult") && aname.StartsWith("Vk")) {
					aname = aname.Substring(2);
				}

				if (!Values.TryGetValue(aname, out _)) {
					Error($"Unknown alias '{rawValue.Alias}' for enum value '{rawValue.Name}'");
					return false;
				}
				value = new(vname, 0, aname);
			}
			else {
				value = new(vname, rawValue.IntValue, null);
			}
			Values.TryAdd(value.Name, value);
			if (Args.Debug) {
				Debug($"    Processed '{rawValue.Name}' into '{value.Name}'");
			}
			return true;
		}
		else {
			Error($"The enum value {rawValue.Name} does not match enum {Name}");
			return false;
		}
	}

	public void Generate(StringBuilder buffer)
	{
		// Open enum
		if (IsBitmask) {
			buffer.AppendLine("[Flags]");
		}
		buffer.Append("public enum ");
		buffer.Append(Name);
		buffer.AppendLine(" : int");
		buffer.AppendLine("{");
		if (IsBitmask) {
			buffer.AppendLine("\tNOFLAGS = 0,");
		}

		foreach ((_, var value) in Values) {
			buffer.Append('\t');
			buffer.Append(value.Name);
			buffer.Append(" = ");
			if (value.Alias is not null) {
				buffer.Append(value.Alias);
			}
			else {
				buffer.Append(value.IntValue);
			}
			buffer.AppendLine(",");
		}

		// Close enum
		buffer.AppendLine("}");
		buffer.AppendLine();
	}
}
