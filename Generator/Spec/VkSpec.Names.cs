﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace VVK;


public partial class VkSpec
{
	// Builtin type
	public record BuiltinType(string SpecName, string GenName, uint Size, bool CanFix);
	// Special type representing a function pointer
	public record FunctionPointer();

	// Converts the spec enum value name format "VK_ENUM_NAME_VALUE_NAME_EXT" into format "ValueNameEXT"
	// Keeps the extension if present
	// Removes the leading enum name if it matches, otherwise does not remove the enum name
	// Removes the "_BIT" part if the enum is a bitmask
	// If the resulting name starts with a digit, an "E" is prepended
	public string SanitizeEnumValueName(string valueName, VkEnum @enum)
	{
		var parts = valueName.Split('_', StringSplitOptions.RemoveEmptyEntries);

		// Check for suffix components
		int trimEnd = 0;
		bool hasVendor = Vendors.TryGetValue(parts[^1], out var vendor);
		if (hasVendor) {
			trimEnd += 1;
		}
		if (@enum.IsBitmask && (parts.Length > 1) && (parts[hasVendor ? ^2 : ^1] == "BIT")) {
			trimEnd += 1;
		}

		// Build the new C# name
		Span<char> buf = stackalloc char[valueName.Length];
		int offset = 0;
		for (int pi = 0; pi < (parts.Length - trimEnd); ++pi) {
			var part = parts[pi];
			buf[offset++] = Char.ToUpperInvariant(part[0]);
			foreach (var ch in part.Skip(1)) {
				buf[offset++] = Char.ToLowerInvariant(ch);
			}
		}

		// Append the extension if needed
		if (hasVendor && (@enum.Vendor.Name != vendor!.Name)) {
			foreach (var ch in vendor.Name) {
				buf[offset++] = ch;
			}
		}

		// Return the new name
		string newString = buf.StartsWith(@enum.BaseName)
			? new(buf.Slice(@enum.BaseName.Length, offset - @enum.BaseName.Length))
			: new(buf.Slice(0, offset));
		return Char.IsDigit(newString[0]) ? ("E" + newString) : newString;
	}

	// Gets the type object associated with the type name
	// Return type can be VkSpec.BuiltinType, VkSpec.VoidPtr, VkStruct, VkEnum, VkHandle, or null
	public object? GetType(string typeName)
	{
		// Look for builtins first
		if (BUILTIN_TYPES.TryGetValue(typeName, out var biType)) {
			return biType;
		}

		// Get vendor and perform lookup
		var vendor = GetTypeVendor(typeName);
		if (vendor.Handles.TryGetValue(typeName, out var vkHandle)) {
			return vkHandle;
		}
		else if (vendor.Enums.TryGetValue(typeName, out var vkEnum)) {
			return vkEnum;
		}
		else if (vendor.Structs.TryGetValue(typeName, out var vkStruct)) {
			return vkStruct;
		}
		else if (typeName.Contains("FlagBits") && 
			vendor.Enums.TryGetValue(typeName.Replace("FlagBits", "Flags"), out var flagsEnum)) {
			return flagsEnum;
		}
		else if (typeName.StartsWith("PFN_")) {
			return new FunctionPointer();
		}
		else {
			return null;
		}
	}

	public static readonly Dictionary<string, BuiltinType> BUILTIN_TYPES = new() {
		// Builtin types
		{ "uint8_t",  new("uint8_t", "byte", 1, true) },
		{ "int8_t",   new("int8_t", "sbyte", 1, true) },
		{ "uint16_t", new("uint16_t", "ushort", 2, true) },
		{ "int16_t",  new("int16_t", "short", 2, true) },
		{ "int",      new("int", "int", 4, true) },
		{ "unsigned", new("unsigned", "uint", 4, true) },
		{ "uint32_t", new("uint32_t", "uint", 4, true) },
		{ "int32_t",  new("int32_t", "int", 4, true) },
		{ "uint64_t", new("uint64_t", "ulong", 8, true) },
		{ "int64_t",  new("int64_t", "long", 8, true) },
		{ "char",     new("char", "byte", 1, true) },
		{ "size_t",   new("size_t", "ulong", 8, true) },
		{ "float",    new("float", "float", 4, true) },
		{ "double",   new("double", "double", 8, true) },
		{ "void",     new("void", "void", 8, true) }, // Only used in void*
		// Vulkan API typedefs
		{ "VkSampleMask",      new("VkSampleMask", "uint", 4, true) },
		{ "VkBool32",          new("VkBool32", "VkBool32", 4, false) },
		{ "VkDeviceSize",      new("VkDeviceSize", "ulong", 8, true) },
		{ "VkDeviceAddress",   new("VkDeviceAddress", "ulong", 8, true) },
		{ "VkRemoteAddressNV", new("VkRemoteAddressNV", "void", 8, true) },
		// Platform types (use 'void' to show that the type needs to be used as a pointer field only)
		// The docs for the rust ash library is a good place to look these up
		{ "HANDLE",              new("HANDLE", "IntPtr", 8, false) },
		{ "HINSTANCE",           new("HINSTANCE", "IntPtr", 8, false) },
		{ "HWND",                new("HWND", "IntPtr", 8, false) },
		{ "HMONITOR",            new("HMONITOR", "IntPtr", 8, false) },
		{ "SECURITY_ATTRIBUTES", new("SECURITY_ATTRIBUTES", "void", 8, false) },
		{ "DWORD",               new("DWORD", "uint", 4, true) },
		{ "LPCWSTR",             new("LPCWSTR", "IntPtr", 8, false) },
		{ "AHardwareBuffer",     new("AHardwareBuffer", "void", 8, false) },
		{ "ANativeWindow",       new("ANativeWindow", "void", 8, false) },
		{ "zx_handle_t",         new("zx_handle_t", "uint", 4, true) },
		{ "GgpStreamDescriptor", new("GgpStreamDescriptor", "uint", 4, true) },
		{ "GgpFrameToken",       new("GgpFrameToken", "ulong", 8, true) },
		{ "wl_display",          new("wl_display", "void", 8, false) },
		{ "wl_surface",          new("wl_surface", "void", 8, false) },
		{ "Display",             new("Display", "void", 8, false) },
		{ "VisualID",            new("VisualID", "uint", 4, true) },
		{ "Window",              new("Window", "ulong", 8, true) },
		{ "RROutput",            new("RROutput", "ulong", 8, true) },
		{ "xcb_connection_t",    new("xcb_connection_t", "void", 8, false) },
		{ "xcb_visualid_t",      new("xcb_visualid_t", "uint", 4, true) },
		{ "xcb_window_t",        new("xcb_window_t", "uint", 4, true) },
		{ "IDirectFB",           new("IDirectFB", "void", 8, false) },
		{ "IDirectFBSurface",    new("IDirectFBSurface", "void", 8, false) },
		{ "CAMetalLayer",        new("CAMetalLayer", "void", 8, false) },
		{ "_screen_context",     new("_screen_context", "void", 8, false) },
		{ "_screen_window",      new("_screen_window", "void", 8, false) },
		// Video extension types
		// Currently not correctly implemented, and treated as opaque types where needed
		{ "StdVideoH264ProfileIdc", new("StdVideoH264ProfileIdc", "uint", 4, true) },
		{ "StdVideoH265ProfileIdc", new("StdVideoH265ProfileIdc	", "uint", 4, true) },
		{ "StdVideoH264SequenceParameterSet", new("StdVideoH264SequenceParameterSet", "void", 8, false) },
		{ "StdVideoH265SequenceParameterSet", new("StdVideoH265SequenceParameterSet", "void", 8, false) },
		{ "StdVideoH264PictureParameterSet", new("StdVideoH264PictureParameterSet", "void", 8, false) },
		{ "StdVideoH265PictureParameterSet", new("StdVideoH265PictureParameterSet", "void", 8, false) },
		{ "StdVideoDecodeH264PictureInfo", new("StdVideoDecodeH264PictureInfo", "void", 8, false) },
		{ "StdVideoDecodeH265PictureInfo", new("StdVideoDecodeH265PictureInfo", "void", 8, false) },
		{ "StdVideoDecodeH264ReferenceInfo", new("StdVideoDecodeH264ReferenceInfo", "void", 8, false) },
		{ "StdVideoDecodeH265ReferenceInfo", new("StdVideoDecodeH265ReferenceInfo", "void", 8, false) },
		{ "StdVideoDecodeH264Mvc", new("StdVideoDecodeH264Mvc", "void", 8, false) },
		{ "StdVideoEncodeH264PictureInfo", new("StdVideoEncodeH264PictureInfo", "void", 8, false) },
		{ "StdVideoEncodeH264SliceHeader", new("StdVideoEncodeH264SliceHeader", "void", 8, false) },
		{ "StdVideoH265VideoParameterSet", new("StdVideoH265VideoParameterSet", "void", 8, false) },
		{ "StdVideoEncodeH265ReferenceInfo", new("StdVideoEncodeH265ReferenceInfo", "void", 8, false) },
		{ "StdVideoEncodeH265ReferenceModifications", new("StdVideoEncodeH265ReferenceModifications", "void", 8, false) },
		{ "StdVideoEncodeH265SliceHeader", new("StdVideoEncodeH265SliceHeader", "void", 8, false) },
		{ "StdVideoEncodeH265PictureInfo", new("StdVideoEncodeH265PictureInfo", "void", 8, false) },
	};
}
