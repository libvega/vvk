﻿/*
 * MIT License (MIT) - Copyright (c) 2022 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace VVK;


public partial class VkSpec
{
	// Special types that do not need to be generated
	public static readonly IReadOnlyCollection<string> NO_GENERATE = new string[] { 
		"VkBaseOutStructure", "VkBaseInStructure"
	};


	private static readonly StringBuilder _Buffer = new();

	// Top-level generation function
	public bool Generate()
	{
		if (Args.Verbose) {
			Info("Generating output");
		}

		// Create the output directory if needed
		DirectoryInfo dirInfo;
		try {
			dirInfo = new(Args.OutputPath);
		}
		catch {
			Error($"Invalid output path '{Args.OutputPath}'");
			return false;
		}
		if (!dirInfo.Exists) {
			dirInfo.Create();
		}
		else if (!dirInfo.Attributes.HasFlag(FileAttributes.Directory)) {
			Error($"Output path '{Args.OutputPath}' already exists and is not a directory");
			return false;
		}

		// Generate top-level files
		GenerateConstants(dirInfo.FullName, this);
		GenerateExtensions(dirInfo.FullName, this);

		// Loop over each of the vendors and generate the types
		foreach ((var vendorName, var vendor) in Vendors) {
			if (Args.Verbose) {
				Info($"Generating for vendor '{vendor.Name}'");
			}

			// Vendor directory
			var vendorPath = Path.Combine(Args.OutputPath, vendor.IsCore ? "" : vendor.Name);
			dirInfo = new(vendorPath);
			if (!dirInfo.Exists) {
				dirInfo.Create();
			}
			else if (!dirInfo.Attributes.HasFlag(FileAttributes.Directory)) {
				Error($"Invalid directory for vendor '{vendor.Name}' - not a directory");
				return false;
			}

			// Generate types
			GenerateEnums(vendorPath, vendor);
			GenerateStructs(vendorPath, vendor);
			GenerateHandles(vendorPath, vendor);
		}

		// Generate the commands
		GenerateCommands(Args.OutputPath, this);

		return true;
	}

	private static void GenerateConstants(string rootDir, VkSpec spec)
	{
		if (Args.Verbose) {
			Info("Generating API constants");
		}

		_Buffer.Clear();

		// Open class
		WriteClassAccessGuard(_Buffer);
		_Buffer.AppendLine("static partial class API");
		_Buffer.AppendLine("{");
		
		foreach (var @const in spec.Constants.Values) {
			_Buffer.Append("\tpublic const ");
			_Buffer.Append(@const.Type);
			_Buffer.Append(' ');
			_Buffer.Append(@const.Name);
			_Buffer.Append(" = ");
			_Buffer.Append(@const.ValueString);
			_Buffer.AppendLine(";");
		}

		// Close class
		_Buffer.AppendLine("}");

		// Write file
		using var writer = StartCodeFile(Path.Combine(rootDir, "API.Constants.cs"));
		writer.Write(_Buffer.ToString());
	}

	private static void GenerateExtensions(string rootDir, VkSpec spec)
	{
		static void _Enumerate(IEnumerable<Extension> exts)
		{
			foreach (var ext in exts) {
				_Buffer.Append("\t\tif (");
				_Buffer.Append(ext.ExtName);
				_Buffer.Append(") yield return API.");
				_Buffer.Append(ext.VarName);
				_Buffer.AppendLine(";");
			}
		}
		static void _SetName(IEnumerable<Extension> exts)
		{
			_Buffer.AppendLine("\t\tswitch (extName)");
			_Buffer.AppendLine("\t\t{");
			foreach (var ext in exts) {
				_Buffer.Append("\t\t\tcase API.");
				_Buffer.Append(ext.VarName);
				_Buffer.Append(": ");
				_Buffer.Append(ext.ExtName);
				_Buffer.AppendLine(" = enable; break;");
			}
			_Buffer.AppendLine("\t\t\tdefault: throw new ArgumentException($\"Unknown extension '{extName}'\", nameof(extName));");
			_Buffer.AppendLine("\t\t}");
		}
		static void _GetName(IEnumerable<Extension> exts)
		{
			_Buffer.AppendLine("\t\tswitch (extName)");
			_Buffer.AppendLine("\t\t{");
			foreach (var ext in exts) {
				_Buffer.Append("\t\t\tcase API.");
				_Buffer.Append(ext.VarName);
				_Buffer.Append(": return ");
				_Buffer.Append(ext.ExtName);
				_Buffer.AppendLine(";");
			}
			_Buffer.AppendLine("\t\t\tdefault: throw new ArgumentException($\"Unknown extension '{extName}'\", nameof(extName));");
			_Buffer.AppendLine("\t\t}");
		}

		if (Args.Verbose) {
			Info("Generating extensions");
		}

		// Write the extension name constants
		{
			_Buffer.Clear();
			WriteClassAccessGuard(_Buffer);
			_Buffer.AppendLine("static partial class API");
			_Buffer.AppendLine("{");

			// Write constants
			foreach ((var extName, var ext) in spec.Extensions) {
				_Buffer.Append("\tpublic const string ");
				_Buffer.Append(ext.VarName);
				_Buffer.Append(" = \"");
				_Buffer.Append(ext.ExtName);
				_Buffer.AppendLine("\";");
			}

			// Write file
			_Buffer.AppendLine("}");
			using var writer = StartCodeFile(Path.Combine(rootDir, "API.Extensions.cs"));
			writer.Write(_Buffer.ToString());
		}

		// Write the instance extension set type
		{
			_Buffer.Clear();
			WriteClassAccessGuard(_Buffer);
			_Buffer.AppendLine("sealed class InstanceExtensionSet");
			_Buffer.AppendLine("{");

			// Write fields
			foreach (var ext in spec.Extensions.Values.Where(e => !e.IsDevice)) {
				_Buffer.Append("\tpublic bool ");
				_Buffer.Append(ext.ExtName);
				_Buffer.AppendLine(" = false;");
			}
			_Buffer.AppendLine();

			// Write ctor
			_Buffer.AppendLine("\tpublic InstanceExtensionSet() { }");
			_Buffer.AppendLine();
			_Buffer.AppendLine("\tpublic InstanceExtensionSet(IEnumerable<string> extList)");
			_Buffer.AppendLine("\t{");
			_Buffer.AppendLine("\t\tforeach (var name in extList) {");
			_Buffer.AppendLine("\t\t\tSet(name, true);");
			_Buffer.AppendLine("\t\t}");
			_Buffer.AppendLine("\t}");
			_Buffer.AppendLine();

			// Write tolist
			_Buffer.AppendLine("\tpublic List<string> ToList() => EnumerateEnabled().ToList();");
			_Buffer.AppendLine();

			// Write enumerateenabled
			_Buffer.AppendLine("\tpublic IEnumerable<string> EnumerateEnabled()");
			_Buffer.AppendLine("\t{");
			_Enumerate(spec.Extensions.Values.Where(e => !e.IsDevice));
			_Buffer.AppendLine("\t}");
			_Buffer.AppendLine();

			// Write set
			_Buffer.AppendLine("\tpublic void Set(string extName, bool enable)");
			_Buffer.AppendLine("\t{");
			_SetName(spec.Extensions.Values.Where(e => !e.IsDevice));
			_Buffer.AppendLine("\t}");
			_Buffer.AppendLine();

			// Write get
			_Buffer.AppendLine("\tpublic bool Get(string extName)");
			_Buffer.AppendLine("\t{");
			_GetName(spec.Extensions.Values.Where(e => !e.IsDevice));
			_Buffer.AppendLine("\t}");

			_Buffer.AppendLine("}");

			// Write file
			using var writer = StartCodeFile(Path.Combine(rootDir, "InstanceExtensionSet.cs"));
			writer.Write(_Buffer.ToString());
		}

		// Write the device extension set type
		{
			_Buffer.Clear();
			WriteClassAccessGuard(_Buffer);
			_Buffer.AppendLine("sealed class DeviceExtensionSet");
			_Buffer.AppendLine("{");

			// Write fields
			foreach (var ext in spec.Extensions.Values.Where(e => e.IsDevice)) {
				_Buffer.Append("\tpublic bool ");
				_Buffer.Append(ext.ExtName);
				_Buffer.AppendLine(" = false;");
			}
			_Buffer.AppendLine();

			// Write ctor
			_Buffer.AppendLine("\tpublic DeviceExtensionSet() { }");
			_Buffer.AppendLine();
			_Buffer.AppendLine("\tpublic DeviceExtensionSet(IEnumerable<string> extList)");
			_Buffer.AppendLine("\t{");
			_Buffer.AppendLine("\t\tforeach (var name in extList) {");
			_Buffer.AppendLine("\t\t\tSet(name, true);");
			_Buffer.AppendLine("\t\t}");
			_Buffer.AppendLine("\t}");
			_Buffer.AppendLine();

			// Write tolist
			_Buffer.AppendLine("\tpublic List<string> ToList() => EnumerateEnabled().ToList();");
			_Buffer.AppendLine();

			// Write enumerateenabled
			_Buffer.AppendLine("\tpublic IEnumerable<string> EnumerateEnabled()");
			_Buffer.AppendLine("\t{");
			_Enumerate(spec.Extensions.Values.Where(e => e.IsDevice));
			_Buffer.AppendLine("\t}");
			_Buffer.AppendLine();

			// Write set
			_Buffer.AppendLine("\tpublic void Set(string extName, bool enable)");
			_Buffer.AppendLine("\t{");
			_SetName(spec.Extensions.Values.Where(e => e.IsDevice));
			_Buffer.AppendLine("\t}");
			_Buffer.AppendLine();

			// Write get
			_Buffer.AppendLine("\tpublic bool Get(string extName)");
			_Buffer.AppendLine("\t{");
			_GetName(spec.Extensions.Values.Where(e => e.IsDevice));
			_Buffer.AppendLine("\t}");

			_Buffer.AppendLine("}");

			// Write file
			using var writer = StartCodeFile(Path.Combine(rootDir, "DeviceExtensionSet.cs"));
			writer.Write(_Buffer.ToString());
		}
	}

	private static void GenerateEnums(string dirPath, VkVendor vendor)
	{
		if (vendor.Enums.Count == 0) {
			if (Args.Debug) {
				Debug("  Skipping enums");
			}
			return;
		}
		else if (Args.Debug) {
			Debug("  Generating enums");
		}

		_Buffer.Clear();
		WriteClassAccessGuard(_Buffer);
		_Buffer.AppendLine("static partial class API");
		_Buffer.AppendLine("{");
		_Buffer.AppendLine();
		foreach (var @enum in vendor.Enums.Values) {
			if (NO_GENERATE.Contains(@enum.Name)) {
				continue;
			}
			@enum.Generate(_Buffer);
		}
		_Buffer.AppendLine();
		_Buffer.AppendLine("}");

		// Write the file
		using var writer = StartCodeFile(Path.Combine(dirPath, $"{vendor.Name}.Enums.cs"));
		writer.Write(_Buffer.ToString());
	}

	private static void GenerateStructs(string dirPath, VkVendor vendor)
	{
		if (vendor.Structs.Count == 0) {
			if (Args.Debug) {
				Debug("  Skipping structs");
			}
			return;
		}
		else if (Args.Debug) {
			Debug("  Generating structs");
		}

		_Buffer.Clear();
		WriteClassAccessGuard(_Buffer);
		_Buffer.AppendLine("static partial class API");
		_Buffer.AppendLine("{");
		_Buffer.AppendLine();
		foreach (var @struct in vendor.Structs.Values) {
			if (NO_GENERATE.Contains(@struct.Name)) {
				continue;
			}
			@struct.Generate(_Buffer);
		}
		_Buffer.AppendLine();
		_Buffer.AppendLine("}");

		// Write the file
		using var writer = StartCodeFile(Path.Combine(dirPath, $"{vendor.Name}.Structs.cs"));
		writer.Write(_Buffer.ToString());
	}

	private static void GenerateHandles(string dirPath, VkVendor vendor)
	{
		if (vendor.Structs.Count == 0) {
			if (Args.Debug) {
				Debug("  Skipping handles");
			}
			return;
		}
		else if (Args.Debug) {
			Debug("  Generating handles");
		}

		_Buffer.Clear();
		WriteClassAccessGuard(_Buffer);
		_Buffer.AppendLine("static partial class API");
		_Buffer.AppendLine("{");
		_Buffer.AppendLine();
		foreach (var handle in vendor.Handles.Values) {
			if (NO_GENERATE.Contains(handle.Name)) {
				continue;
			}
			handle.Generate(_Buffer);
		}
		_Buffer.AppendLine();
		_Buffer.AppendLine("}");

		// Write the file
		using var writer = StartCodeFile(Path.Combine(dirPath, $"{vendor.Name}.Handles.cs"));
		writer.Write(_Buffer.ToString());
	}

	private static void GenerateCommands(string dirPath, VkSpec spec)
	{
		// Start the commands file
		_Buffer.Clear();
		WriteClassAccessGuard(_Buffer);
		_Buffer.AppendLine("static unsafe partial class API");
		_Buffer.AppendLine("{");

		// Write the function fields
		foreach (var cmd in spec.Vendors.Values.SelectMany(v => v.Commands.Values)) {
			if (NO_GENERATE.Contains(cmd.Name)) {
				continue;
			}
			cmd.GenerateField(_Buffer);
		}
		_Buffer.AppendLine();

		// Generate the global loads
		_Buffer.AppendLine("\tprivate static partial void LoadGlobals()");
		_Buffer.AppendLine("\t{");
		foreach (var cmd in spec.Vendors.Values.SelectMany(v => v.Commands.Values)) {
			if (NO_GENERATE.Contains(cmd.Name)) {
				continue;
			}
			if (cmd.ObjectLevel == VkCommand.Level.Global) {
				cmd.GenerateLoad(spec, _Buffer);
			}
		}
		_Buffer.AppendLine("\t}");
		_Buffer.AppendLine();

		// Generate the instance loads
		_Buffer.AppendLine("\tinternal static partial void LoadInstance(VkInstance inst)");
		_Buffer.AppendLine("\t{");
		foreach (var cmd in spec.Vendors.Values.SelectMany(v => v.Commands.Values)) {
			if (NO_GENERATE.Contains(cmd.Name)) {
				continue;
			}
			if (cmd.ObjectLevel == VkCommand.Level.Instance) {
				cmd.GenerateLoad(spec, _Buffer);
			}
		}
		_Buffer.AppendLine("\t}");
		_Buffer.AppendLine();

		// Generate the device loads
		_Buffer.AppendLine("\tinternal static partial void LoadDevice(VkDevice device)");
		_Buffer.AppendLine("\t{");
		foreach (var cmd in spec.Vendors.Values.SelectMany(v => v.Commands.Values)) {
			if (NO_GENERATE.Contains(cmd.Name)) {
				continue;
			}
			if (cmd.ObjectLevel == VkCommand.Level.Device) {
				cmd.GenerateLoad(spec, _Buffer);
			}
		}
		_Buffer.AppendLine("\t}");
		_Buffer.AppendLine();

		// Generate the public functions
		foreach (var cmd in spec.Vendors.Values.SelectMany(v => v.Commands.Values)) {
			if (NO_GENERATE.Contains(cmd.Name)) {
				continue;
			}
			cmd.GenerateCall(_Buffer);
		}

		// Close the instance file
		_Buffer.AppendLine("}");

		// Write the instance file
		using var writer = StartCodeFile(Path.Combine(dirPath, "API.Functions.cs"));
		writer.Write(_Buffer.ToString());
	}

	private static StreamWriter StartCodeFile(string path)
	{
		var writer = new StreamWriter(File.Open(path, FileMode.Create, FileAccess.Write, FileShare.None));

		// Write the standard header
		writer.WriteLine("/*");
		writer.WriteLine(" * MIT License (MIT) - Copyright (c) 2021-2022 Sean Moss");
		writer.WriteLine(" * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'");
		writer.WriteLine(" * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.");
		writer.WriteLine(" */");
		writer.WriteLine();
		writer.WriteLine("using System;");
		writer.WriteLine("using System.Text;");
		writer.WriteLine("using System.Collections.Generic;");
		writer.WriteLine("using System.Runtime.InteropServices;");
		writer.WriteLine("using System.Runtime.CompilerServices;");
		writer.WriteLine("using System.Linq;");
		writer.WriteLine("using static Vulkan.InternalUtilities;");
		writer.WriteLine();
		writer.WriteLine("namespace Vulkan;");
		writer.WriteLine();
		writer.WriteLine();

		return writer;
	}

	// Writes the define guard to define the access level of the API types
	private static void WriteClassAccessGuard(StringBuilder builder)
	{
		builder.AppendLine("#if VVK_API_INTERNAL");
		builder.AppendLine("internal");
		builder.AppendLine("#else");
		builder.AppendLine("public");
		builder.AppendLine("#endif");
	}
}
