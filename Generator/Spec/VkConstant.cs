﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;

namespace VVK;


// Represents a processed constant value
public class VkConstant
{
	#region Fields
	// The constant name (without the "VK_")
	public readonly string Name;
	// The C# constant type string
	public readonly string Type;
	// The C# value literal string
	public readonly string ValueString;
	#endregion // Fields

	public VkConstant(string name, string type, string value)
	{
		Name = name;
		Type = type;
		ValueString = value;
	}

	private static readonly Regex BITNOT_PATTERN = new(@"^\(?~(?<IntVal>\d+)U(?<ULong>LL)?(-(?<Offset>\d+))?\)?$");
	public static bool TryProcess(VkSpec vkSpec, RawConstant rawConst, [NotNullWhen(true)] out VkConstant? vkConst)
	{
		vkConst = null;

		// Deduct the type and value string
		string type, value = rawConst.ValueOrAlias;
		if (value.StartsWith("VK_")) {
			if (!vkSpec.Constants.TryGetValue(value, out var aliasConst)) {
				Error($"Unknown constant alias '{rawConst.ValueOrAlias}' for constant '{rawConst.Name}'");
				return false;
			}
			type = aliasConst.Type;
		}
		else if (value.Contains('.') || value.EndsWith('f')) {
			type = value.EndsWith('f') ? "float" : "double";
		}
		else if (value.All(ch => Char.IsDigit(ch))) {
			type = "uint";
		}
		else if ((BITNOT_PATTERN.Match(value) is Match match) && match.Success) {
			var isLong = match.Groups["ULong"].Success;
			int offset = 0;
			if (match.Groups["Offset"].Success) {
				offset = Int32.Parse(match.Groups["Offset"].Value);
			}
			var intValue = isLong ? UInt64.Parse(match.Groups["IntVal"].Value) : UInt32.Parse(match.Groups["IntVal"].Value);
			type = isLong ? "ulong" : "uint";
			value = "(~" + intValue.ToString() + "u" + (isLong ? "l" : "") + ")" + ((offset != 0) ? $" - {offset}" : String.Empty);
		}
		else {
			throw new NotImplementedException($"Unknown value for constant '{rawConst.Name}' = {rawConst.ValueOrAlias}");
		}

		vkConst = new(rawConst.Name, type, value);
		return true;
	}
}
