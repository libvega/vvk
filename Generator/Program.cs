﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;

namespace VVK;


static class Program
{
	static int Main(string[] args)
	{
		// Parse arguments and print help
		if (!Args.Parse(args)) {
			return -1;
		}
		if (Args.Help) {
			Args.PrintHelp();
			return 0;
		}

		// Load the raw spec
		RawSpec? rawSpec;
#if DEBUG
		if (!RawSpec.TryParse(Args.InputFile, out rawSpec)) {
			Error("Failed to parse spec file");
			return -2;
		}
#else
		try {
			if (!RawSpec.TryParse(Args.InputFile, out rawSpec)) {
				Error("Failed to parse spec file");
				return -2;
			}
		}
		catch (Exception ex) {
			Error($"Failed to parse spec file (unhandled exception: '{ex.Message}')");
			return -2;
		}
#endif // DEBUG

		// Process the spec
		VkSpec? vkSpec;
#if DEBUG
		if (!VkSpec.TryProcess(rawSpec, out vkSpec)) {
			Error("Failed to process spec");
			return -3;
		}
		if (!vkSpec.Generate()) {
			Error("Failed to generate spec");
			return -4;
		}
#else
		try {
			if (!VkSpec.TryProcess(rawSpec, out vkSpec)) {
				Error("Failed to process spec");
				return -3;
			}
		}
		catch (Exception ex) {
			Error($"Failed to process spec (unhandled exception: '{ex.Message}')");
			return -3;
		}
		try {
			if (!vkSpec.Generate()) {
				Error("Failed to generate spec");
				return -4;
			}
		}
		catch (Exception ex) {
			Error($"Failed to generate spec (unhandled exception: '{ex.Message}')");
			return -4;
		}
#endif // DEBUG

		// Good generation
		return 0;
	}
}
