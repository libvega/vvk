﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Linq;

namespace VVK;


public static class Args
{
	#region Options
	// If the help flag was specified
	public static bool Help { get; private set; } = false;

	// The input spec file
	public static string InputFile { get; private set; } = "./vk.xml";

	// If the output should be verbose
	public static bool Verbose { get; private set; } = false;

	// If the output should dump detailed debug info to the console
	public static bool Debug { get; private set; } = false;

	// The output path for the generated files
	public static string OutputPath { get; private set; } = "./VVK";

	// List of gen/skip vendors, if specified
	public static string[]? VendorList { get; private set; } = null;

	// If VendorList is not null, this will be true if the list is skipped, false if generated
	public static bool VendorSkip { get; private set; } = false;
	#endregion // Options

	// Parse the command line arguments
	public static bool Parse(string[] rawargs)
	{
		// Normalizes flags to be all lower-case and start with a single '-', returns String.Empty to skip
		static string normalize_(string arg)
		{
			bool isparam = (arg[0] == '-');
			bool iswide = (arg.Length > 1) && (arg[1] == '-');
			if ((isparam && arg.Length == 1) || (iswide && arg.Length == 2)) {
				return String.Empty;
			}
			return isparam ? $"-{arg.Substring(iswide ? 2 : 1).ToLowerInvariant()}" : arg;
		}

		// Normalize arguments
		var args = rawargs.Select(normalize_).Where(s => !String.IsNullOrWhiteSpace(s)).ToList();
		if (args.Count == 0) {
			return true;
		}

		// Check for help flag and return immediately
		if (args.Contains("-h") || args.Contains("-help") || args.Contains("-?")) {
			Help = true;
			return true;
		}

		// Process parameters and arguments
		for (int ai = 0; ai < args.Count; ) {
			bool last = ai == (args.Count - 1);
			switch (args[ai]) 
			{
			case "-v": 
			case "-verbose": Verbose = true; ++ai; break;
			case "-d":
			case "-debug": Verbose = true; Debug = true; ++ai; break;
			case "-o":
			case "-output": {
				if (last) {
					Error($"No path specified for argument {args[ai]}");
					return false;
				}
				OutputPath = args[ai + 1];
				ai += 2;
			} break;
			case "-g":
			case "-gen":
			case "-vendors":
			case "-s":
			case "-skip": {
				if (last) {
					Error($"No vendor list specified for {args[ai]}");
					return false;
				}
				VendorList = args[ai + 1].Split(',', StringSplitOptions.RemoveEmptyEntries);
				VendorSkip = (args[ai] == "-s") || (args[ai] == "-skip");
				ai += 2;
			} break;
			default: {
				if (args[ai].StartsWith('-')) {
					// Unknown argument
				}
				else {
					InputFile = args[ai];
				}
				++ai;
			} break;
			}
		}

		// Done parsing
		return true;
	}

	// Print the help text
	public static void PrintHelp()
	{
		Console.WriteLine();
		Console.WriteLine(
			"Usage: VVKGen.exe [args] <spec_file>\n" +
			"<spec_file> is the vk.xml file providing the Vulkan specification\n" +
			"[args] can be specified with `-` or `--`, and is one or more of:\n" +
			"   h;help;?             - Print this help and exit.\n" +
			"   v;verbose            - Use verbose output during generation.\n" +
			"   d;debug              - Dump detailed process debug info (implies -v).\n" +
			"   o;output             - The directory to generate files into.\n" +
			"   g;gen;vendors <list> - Provide a comma delimited list of vendors to\n" +
			"                          generate, only these will be generated\n" +
			"   s;skip <list>        - Provide a comma delimited list of vendors to\n" +
			"                          skip, all vendors but these will be generated"
		);
		Console.WriteLine();
	}
}
