﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Xml;

namespace VVK;


// Object handle type from the spec
public class RawHandle
{
	#region Fields
	// The handle type name
	public readonly string Name;
	// The handle parent type name
	public readonly string? Parent;
	// The name of the VkObjectType enum 
	public readonly string TypeEnum;
	#endregion // Fields

	private RawHandle(string name, string? parent, string typeEnum)
	{
		Name = name;
		Parent = parent;
		TypeEnum = typeEnum;
	}

	public static bool TryParse(XmlNode xml, [NotNullWhen(true)] out RawHandle? handle)
	{
#if !DEBUG
		try {
#endif
			handle = Parse(xml);
			return true;
#if !DEBUG
		}
		catch (Exception ex) {
			Error(ex.Message);
			handle = null;
			return false;
		}
#endif
	}

	private static RawHandle Parse(XmlNode xml)
	{
		// Get name
		string name;
		if (xml.Attributes!["name"] is XmlAttribute nameAttr) {
			name = nameAttr.Name;
		}
		else if (xml.SelectSingleNode("name") is XmlNode nameNode) {
			name = nameNode.InnerText;
		}
		else {
			throw new Exception("Cannot discover handle name");
		}

		// Get object type
		if (xml.Attributes["objtypeenum"] is not XmlAttribute objAttr) {
			throw new Exception("Handle type is missing object type enum");
		}

		return new(name, xml.Attributes["parent"]?.Value, objAttr.Value);
	}
}
