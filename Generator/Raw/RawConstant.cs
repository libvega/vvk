﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Xml;

namespace VVK;


// Raw unprocessed spec constant
public class RawConstant
{
	#region Fields
	// The name of the constant
	public readonly string Name;
	// The raw unprocessed value string (which may be the aliased constant)
	public readonly string ValueOrAlias;

	// If the value is aliased to another constant
	public bool IsAlias => ValueOrAlias.StartsWith("VK_");
	#endregion // Fields

	private RawConstant(string name, string valueOrAlias)
	{
		Name = name;
		ValueOrAlias = valueOrAlias;
	}

	public static bool TryParse(XmlNode xml, [NotNullWhen(true)] out RawConstant? @const)
	{
#if !DEBUG
		try {
#endif
			@const = Parse(xml);
			return true;
#if !DEBUG
		}
		catch (Exception ex) {
			Error(ex.Message);
			@const = null;
			return false;
		}
#endif
	}

	private static RawConstant Parse(XmlNode xml)
	{
		var name = xml.Attributes!["name"]!.Value;
		string? value;
		if (xml.Attributes["value"] is XmlAttribute valAttr) {
			value = valAttr.Value;
		}
		else if (xml.Attributes["alias"] is XmlAttribute alAttr) {
			value = alAttr.Value;
		}
		else {
			throw new Exception("API constant does not have a value");
		}
		return new(name, value);
	}
}
