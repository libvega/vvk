﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Xml;

namespace VVK;


// Unprocessed command (API function) from the spec
// These objects are duplicated for aliases, since special command loading logic is needed later that relies on this
public class RawCommand
{
	// Command parameter record
	public record Param(string Name, string Type, bool Const, uint PtrCount, string? LenParam);

	#region Fields
	// The command name
	public readonly string Name;
	// The return type
	public readonly string ReturnType;
	// The API version or extension required for the command to be loaded
	// Versions are the format "1_x" and extension names are unchanged
	public string Requires = "1_0";
	// If the command is aliased to another, this is the target command name
	public string? Alias = null;

	// Parameters
	public readonly List<Param> Params = new();
	#endregion // Fields

	private RawCommand(string name, string rettype)
	{
		Name = name;
		ReturnType = rettype;
	}

	public static bool TryParse(XmlNode xml, RawSpec spec, [NotNullWhen(true)] out RawCommand? cmd)
	{
#if !DEBUG
		try {
#endif
			cmd = Parse(xml, spec);
			return true;
#if !DEBUG
		}
		catch (Exception ex) {
			Error(ex.Message);
			cmd = null;
			return false;
		}
#endif
	}

	private static RawCommand Parse(XmlNode xml, RawSpec spec)
	{
		// Check for the alias first
		RawCommand cmd;
		if (xml.Attributes?["alias"] is XmlAttribute aliasAttr) {
			var cname = xml.Attributes["name"]!.Value;
			if (!spec.Commands.TryGetValue(aliasAttr.Value, out var aliased)) {
				throw new Exception($"Cannot find aliased command '{aliasAttr.Value}' for command '{cname}'");
			}
			
			cmd = new(cname, aliased.ReturnType);
			foreach (var par in aliased.Params) {
				cmd.Params.Add(par);
			}
			cmd.Alias = aliasAttr.Value;
		}
		else {
			// Name and return type
			var cname = xml.SelectSingleNode("proto")!.SelectSingleNode("name")!.InnerText;
			var retType = xml.SelectSingleNode("proto")!.SelectSingleNode("type")!.InnerText;
			cmd = new(cname, retType);

			// Parameters
			foreach (var parNode in xml.SelectNodes("param")!.OfType<XmlNode>()) {
				var pname = parNode.SelectSingleNode("name")!.InnerText;
				var ptype = parNode.SelectSingleNode("type")!.InnerText;
				var ptrCount = parNode.InnerText.Count(ch => ch == '*');
				var lenParam = parNode.Attributes?["len"]?.Value;
				var @const = parNode.InnerText.StartsWith("const ");
				cmd.Params.Add(new(pname, ptype, @const, (uint)ptrCount, lenParam));
			}
		}

		return cmd;
	}
}
