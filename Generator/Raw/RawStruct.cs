﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;

namespace VVK;


// Represents an unprocessed struct type and its fields from the spec
public class RawStruct
{
	// Struct field types
	public record Field(string Name, string Type);
	public record ArrayField(string Name, string Type, string ArraySize) : Field(Name, Type);
	public record PointerField(string Name, string Type, uint PtrCount) : Field(Name, Type);

	#region Fields
	// Type name
	public readonly string Name;
	// If the type is a union
	public readonly bool IsUnion;
	// If the type is marked as "return only"
	public readonly bool ReturnOnly;

	// For types with the "sType" field, this is the raw type enum value
	public string? StructureType = null;

	// List of types that this struct "extends" (can appear in its pNext chain)
	public readonly List<string> Extends = new();

	// List of type fields
	public readonly List<Field> Fields = new();
	#endregion // Fields

	private RawStruct(string name, bool isunion, bool retonly)
	{
		Name = name;
		IsUnion = isunion;
		ReturnOnly = retonly;
	}

	public static bool TryParse(XmlNode xml, [NotNullWhen(true)] out RawStruct? @struct)
	{
#if !DEBUG
		try {
#endif
			@struct = Parse(xml);
			return true;
#if !DEBUG
		}
		catch (Exception ex) {
			Error(ex.Message);
			@struct = null;
			return false;
		}
#endif
	}

	private static readonly Regex ARRAY_REGEX = new(@"\[([\d_a-zA-Z]+)\]");
	private static RawStruct Parse(XmlNode xml)
	{
		// Get name and type
		var name = xml.Attributes!["name"]!.Value;
		var isUnion = xml.Attributes["category"]!.Value == "union";
		var retOnly = (xml.Attributes["returnedonly"] is XmlAttribute retAttr) && (retAttr.Value == "true");
		RawStruct @struct = new(name, isUnion, retOnly);

		// Get extends
		if (xml.Attributes["structextends"] is XmlAttribute extAttr) {
			@struct.Extends.AddRange(extAttr.Value.Split(','));
		}

		// Iterate over members
		foreach (var memnode in xml.SelectNodes("member")!.OfType<XmlNode>()) {
			// Remove the comment node if present, since it just messes up this logic
			if (memnode.SelectSingleNode("comment") is XmlNode commentNode) {
				commentNode.ParentNode!.RemoveChild(commentNode);
			}

			// Get member info
			var mname = memnode.SelectSingleNode("name")!.InnerText;
			var mtype = memnode.SelectSingleNode("type")!.InnerText;
			var isArray = memnode.InnerText.IndexOf('[') != -1;
			var ptrCount = memnode.InnerText.Count(ch => ch == '*');

			// Switch on field type
			if (isArray) {
				var matches = ARRAY_REGEX.Matches(memnode.InnerText);
				var arrSize = String.Join('*', matches.Select(mat => mat.Groups[1].Value));
				@struct.Fields.Add(new ArrayField(mname, mtype, arrSize));
			}
			else if (ptrCount != 0) {
				@struct.Fields.Add(new PointerField(mname, mtype, (uint)ptrCount));
			}
			else {
				@struct.Fields.Add(new Field(mname, mtype));
			}

			// Save the struct type if present
			if ((mname == "sType") && (mtype == "VkStructureType")) {
				@struct.StructureType = memnode.Attributes!["values"]?.Value;
			}
		}

		return @struct;
	}
}
