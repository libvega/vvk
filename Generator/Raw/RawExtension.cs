﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;

namespace VVK;


// Represents an unprocessed extension loaded from the spec
public class RawExtension
{
	// Enum extension value
	public record EnumValue(RawExtension Extension, string Name, string Enum, int Value, string? Alias);
	// Additional requirement notes for commands
	public record CommandReq(string Command, string Requires);

	#region Fields
	// The extension name (as used to load the extension)
	public readonly string Name;
	// The vendor author for the extension
	public readonly string Vendor;
	// If the extension is a device extension, false implies instance extension
	public readonly bool IsDevice;
	// The extension number
	public readonly int ExtNumber;

	// If the extension is enabled
	public readonly bool IsEnabled;

	// Base enum offset for extension
	public int EnumOffset => 1_000_000_000 + ((ExtNumber - 1) * 1000);

	// The list of enum extension values
	public readonly List<EnumValue> Enums = new();

	// Additional command feature levels
	public readonly List<CommandReq> Commands = new();
	#endregion // Fields

	private RawExtension(string name, string vendor, bool isDevice, int extNum, bool enabled)
	{
		Name = name;
		Vendor = vendor;
		IsDevice = isDevice;
		ExtNumber = extNum;
		IsEnabled = enabled;
	}

	public static bool TryParse(XmlNode xml, [NotNullWhen(true)] out RawExtension? ext)
	{
#if !DEBUG
		try {
#endif
			ext = Parse(xml);
			return true;
#if !DEBUG
		}
		catch (Exception ex) {
			Error(ex.Message);
			ext = null;
			return false;
		}
#endif
	}

	private static readonly Regex VENDOR_REGEX = new(@"^VK_([A-Za-z]+)_.*?$");
	private static RawExtension Parse(XmlNode xml)
	{
		// Create the extension
		var enabled = xml.Attributes!["supported"]!.Value != "disabled";
		if (!enabled) {
			return new("", "", false, 0, false); // Disabled extensions are automatically ignored
		}
		RawExtension ext;
		{
			var name = xml.Attributes["name"]!.Value;
			var extNumber = Int32.Parse(xml.Attributes["number"]!.Value);
			var author = xml.Attributes["author"]?.Value ?? VENDOR_REGEX.Match(name).Groups[1].Value;
			var isDevice = xml.Attributes["type"]!.Value == "device";
			ext = new(name, author, isDevice, extNumber, true);
		}

		// Loop over the required blocks
		foreach (var reqNode in xml.SelectNodes("require")!.OfType<XmlNode>()) {
			// Parse enum extensions
			foreach (var enumNode in reqNode.SelectNodes("enum")!.OfType<XmlNode>()) {
				var extends = enumNode.Attributes!["extends"]?.Value;
				if (extends is null) {
					continue;
				}
				var enumName = enumNode.Attributes["name"]!.Value;
				if (enumNode.Attributes["offset"] is XmlAttribute offAttr) {
					bool isNeg = (enumNode.Attributes["dir"]?.Value ?? "") == "-";
					var value = ext.EnumOffset + Int32.Parse(offAttr.Value);
					ext.Enums.Add(new(ext, enumName, extends, isNeg ? -value : value, null));
				}
				else if (enumNode.Attributes["bitpos"] is XmlAttribute bitAttr) {
					ext.Enums.Add(new(ext, enumName, extends, 1 << Int32.Parse(bitAttr.Value), null));
				}
				else if (enumNode.Attributes["alias"] is XmlAttribute aliasAttr) {
					ext.Enums.Add(new(ext, enumName, extends, 0, aliasAttr.Value));
				}
				else if (enumNode.Attributes["value"] is XmlAttribute valueAttr) {
					ext.Enums.Add(new(ext, enumName, extends, Int32.Parse(valueAttr.Value), null));
				}
				else {
					throw new NotImplementedException("Cannot parse extension enum value");
				}
			}

			// Assign extension requirements to the commands
			foreach (var cmdNode in reqNode.SelectNodes("command")!.OfType<XmlNode>()) {
				ext.Commands.Add(new(cmdNode.Attributes!["name"]!.Value, ext.Name));
			}
		}

		return ext;
	}
}
