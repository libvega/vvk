﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Xml;

namespace VVK;


// Raw complete spec loaded from the xml file, with minimal processing applied
// Step one in the three step load/process/generate pipeline
public class RawSpec
{
	#region Fields
	// List of known vendor tags
	public readonly List<string> Vendors = new();

	// Known enums
	public readonly Dictionary<string, RawEnum> Enums = new();

	// API constants
	public readonly Dictionary<string, RawConstant> Constants = new();

	// Handle types
	public readonly Dictionary<string, RawHandle> Handles = new();

	// Struct/Union types
	public readonly Dictionary<string, RawStruct> Structs = new();

	// Commands (API functions)
	public readonly Dictionary<string, RawCommand> Commands = new();

	// Extensions
	public readonly Dictionary<string, RawExtension> Extensions = new();
	#endregion // Fields

	private RawSpec()
	{

	}

	// Perform parsing of the XML spec
	public static bool TryParse(string specFile, [NotNullWhen(true)] out RawSpec? spec)
	{
		spec = null;

		// Load the xml document
		FileInfo file = new(specFile);
		if (!file.Exists) {
			Error($"Input file '{specFile}' does not exist");
			return false;
		}
		XmlDocument xml = new();
		try {
			xml.Load(file.OpenRead());
		}
		catch (Exception ex) {
			Error($"Input file could not be loaded (error: {ex.Message})");
			return false;
		}

		// Get the root node
		if (xml.DocumentElement?.Name != "registry") {
			Error("Input file does not appear to be the XML Vulkan spec");
			return false;
		}
		var root = xml.DocumentElement!;
		spec = new();

		// Load the vendors
		if (!LoadVendors(root, spec)) {
			return false;
		}

		// Load the types
		if (!LoadTypes(root, spec)) {
			return false;
		}

		// Parse the details for enums and bitmasks
		if (!ParseEnums(root, spec)) {
			return false;
		}

		// Load the commands (API functions)
		if (!LoadCommands(root, spec)) {
			return false;
		}

		// Load the extensions
		if (!LoadExtensions(root, spec)) {
			return false;
		}

		return true;
	}

	private static bool LoadVendors(XmlNode root, RawSpec spec)
	{
		if (Args.Verbose) {
			Info("Loading vendors");
		}
		foreach (var node in root["tags"]!.SelectNodes("tag")!.OfType<XmlElement>()) {
			var name = node!.Attributes["name"]!.Value;
			spec.Vendors.Add(name);
			if (Args.Debug) {
				Debug($"  Found vendor: {name}");
			}
		}
		if (Args.Verbose) {
			Info($"Found {spec.Vendors.Count} vendors");
		}

		return true;
	}

	private static bool LoadTypes(XmlNode root, RawSpec spec)
	{
		if (Args.Verbose) {
			Info("Loading types");
		}

		uint typecount = 0;
		foreach (var typeNode in root.SelectSingleNode("types")!.SelectNodes("type")!.OfType<XmlNode>()) {
			// Skip uncategorized types
			if (typeNode.Attributes!["category"] is not XmlAttribute catAttr) {
				continue;
			}

			// Check for common type attributes
			string typeName;
			if (typeNode.Attributes["name"] is XmlAttribute typeNameAttr) {
				typeName = typeNameAttr.Value;
			}
			else if (typeNode.SelectSingleNode("name") is XmlNode nameNode) {
				typeName = nameNode.InnerText;
			}
			else {
				continue;
			}
			string? aliasName = null;
			if (typeNode.Attributes["alias"] is XmlAttribute aliasAttr) {
				aliasName = aliasAttr.Value;
			}

			// Switch on the type
			switch (catAttr.Value)
			{
			case "bitmask":
			case "enum": {
				if (aliasName is not null) {
					if (!spec.Enums.TryGetValue(aliasName, out var aliasType)) {
						Error($"Unknown alias '{aliasName}' for type '{typeName}'");
						return false;
					}
					spec.Enums.Add(typeName, aliasType);
				}
				else {
					if (typeName.Contains("FlagBits") && 
						spec.Enums.TryGetValue(typeName.Replace("FlagBits", "Flags"), out _)) {
						if (Args.Debug) {
							Debug($"  Skipping duplicate flag bits '{typeName}'");
						}
						continue; // Avoid duplicate FlagBits (which are already added as needed by Flags)
					}
					RawEnum newEnum = new(typeName, catAttr.Value == "bitmask");
					spec.Enums.Add(typeName, newEnum);
					if (typeNode.Attributes["requires"] is XmlAttribute reqAttr) {
						spec.Enums.Add(reqAttr.Value, newEnum);
					}
					if (typeNode.Attributes["bitvalues"] is XmlAttribute bvAttr) {
						spec.Enums.Add(bvAttr.Value, newEnum); // Used by new 64-bit flag types
					}
				}
			} break;
			case "handle": {
				if (aliasName is not null) {
					if (!spec.Handles.TryGetValue(aliasName, out var aliasType)) {
						Error($"Unknown alias '{aliasName}' for '{typeName}'");
						return false;
					}
					spec.Handles.Add(typeName, aliasType);
				}
				else {
					if (!RawHandle.TryParse(typeNode, out var handle)) {
						return false;
					}
					spec.Handles.Add(handle.Name, handle);
				}
			} break;
			case "struct":
			case "union": {
				if (aliasName is not null) {
					if (!spec.Structs.TryGetValue(aliasName, out var aliasType)) {
						Error($"Unknown alias '{aliasName}' for '{typeName}'");
						return false;
					}
					spec.Structs.Add(typeName, aliasType);
				}
				else {
					if (!RawStruct.TryParse(typeNode, out var @struct)) {
						return false;
					}
					spec.Structs.Add(typeName, @struct);
				}
			} break;
			default: continue;
			}

			// Report
			typecount++;
			if (Args.Debug) {
				if (aliasName is not null) {
					Debug($"  Found alias {typeName} -> {aliasName}");
				}
				else {
					Debug($"  Found type {typeName}");
				}
			}
		}

		if (Args.Verbose) {
			Info($"Found {typecount} types");
		}
		return true;
	}

	private static bool ParseEnums(XmlNode root, RawSpec spec)
	{
		if (Args.Verbose) {
			Info("Parsing enums");
		}

		foreach (var node in root.SelectNodes("enums")!.OfType<XmlNode>()) {
			var name = node.Attributes!["name"]!.Value;

			// Special enum containing the API constants
			if (name == "API Constants") {
				foreach (var cnode in node.SelectNodes("enum")!.OfType<XmlNode>()) {
					if (!RawConstant.TryParse(cnode, out var @const)) {
						return false;
					}
					spec.Constants.Add(@const.Name, @const);
				}
				if (Args.Debug) {
					Debug($"  Found {spec.Constants.Count} API constants");
				}
				continue;
			}

			// Get the existing enum and parse
			if (!spec.Enums.TryGetValue(name, out var @enum) && 
					!spec.Enums.TryGetValue(name.Replace("FlagBits", "Flags"), out @enum)) {
				Error($"Could not find existing enum type '{name}' to parse");
				return false;
			}
			if (!RawEnum.TryParse(node, @enum)) {
				return false;
			}
			if (Args.Debug) {
				Debug($"  Parsed {@enum.Values.Count} values for enum {@enum.Name}");
			}
		}

		return true;
	}

	private static bool LoadCommands(XmlNode root, RawSpec spec)
	{
		if (Args.Verbose) {
			Info("Loading commands");
		}

		// Load the commands
		var commands = root.SelectSingleNode("commands")!;
		foreach (var cmdNode in commands.SelectNodes("command")!.OfType<XmlNode>()) {
			if (!RawCommand.TryParse(cmdNode, spec, out var cmd)) {
				return false;
			}
			spec.Commands.Add(cmd.Name, cmd);
			if (Args.Debug) {
				Debug($"  Found command {cmd.Name} ({cmd.Params.Count} params)");
			}
		}

		if (Args.Verbose) {
			Info($"Found {spec.Commands.Count} commands");
		}

		return true;
	}

	private static bool LoadExtensions(XmlNode root, RawSpec spec)
	{
		if (Args.Verbose) {
			Info("Loading core promotions");
		}

		// Load the core requirements (which includes the promotions)
		int promotedEnum = 0;
		foreach (var featureNode in root.SelectNodes("feature")!.OfType<XmlNode>()) {
			var apiVer = featureNode.Attributes!["number"]!.Value switch {
				"1.0" => "1_0",
				"1.1" => "1_1",
				"1.2" => "1_2",
				_ => throw new NotImplementedException("Unsupported feature level in feature level set")
			};

			var reqNodes = featureNode.SelectNodes("require")!.OfType<XmlNode>();
			foreach (var node in reqNodes.SelectMany(node => node.ChildNodes.OfType<XmlNode>())) {
				if (node.Name == "command") {
					var cmdName = node.Attributes!["name"]!.Value;
					if (!spec.Commands.TryGetValue(cmdName, out var cmd)) {
						Error($"Cannot find command '{cmdName}' to assign feature level");
						return false;
					}
					cmd.Requires = apiVer;
					if (Args.Debug) {
						Debug($"  Found promoted command '{cmdName}' (version {apiVer})");
					}
				}
				else if ((node.Name == "enum") && (node.Attributes!["extends"] is not null)) {
					var newValue = LoadCoreEnum(node);
					if (!spec.Enums.TryGetValue(newValue.Enum, out var rawEnum)) {
						Error($"Cannot find enum '{newValue.Enum}' for promoted enum value '{newValue.Name}'");
						return false;
					}
					if (Args.Debug) {
						Debug($"  Found promoted enum value '{newValue.Name}'");
					}
					rawEnum.Values.Add(new(newValue.Name, newValue.Alias, newValue.Value));
					promotedEnum += 1;
				}
			}
		}

		if (Args.Verbose) {
			Info($"Found {promotedEnum} promoted enums");
			Info("Loading extensions");
		}

		// Load the extensions
		foreach (var extNode in root.SelectSingleNode("extensions")!.SelectNodes("extension")!.OfType<XmlNode>()) {
			if (!RawExtension.TryParse(extNode, out var ext)) {
				return false;
			}
			if (!ext.IsEnabled) {
				continue; // Skip disabled extensions
			}
			spec.Extensions.Add(ext.Name, ext);
			if (Args.Debug) {
				Debug($"  Found extension '{ext.Name}'");
			}
		}

		if (Args.Verbose) {
			Info($"Found {spec.Extensions.Count} extensions");
		}

		// Perform extension enum and command assignment
		int enumCount = 0;
		int cmdCount = 0;
		foreach (var ext in spec.Extensions) {
			foreach (var @enum in ext.Value.Enums) {
				if (!spec.Enums.TryGetValue(@enum.Enum, out var rawEnum)) {
					Error($"Could not find enum '{@enum.Enum}' for extension value '{@enum.Name}'");
					return false;
				}
				if (@enum.Alias is not null) {
					rawEnum.Values.Add(new(@enum.Name, @enum.Alias, 0));
				}
				else {
					rawEnum.Values.Add(new(@enum.Name, null, @enum.Value));
				}
				enumCount++;
				if (Args.Debug) {
					Debug($"  Found extension enum '{@enum.Name}' (extends '{rawEnum.Name}')");
				}
			}

			foreach (var cmd in ext.Value.Commands) {
				if (!spec.Commands.TryGetValue(cmd.Command, out var rawCmd)) {
					Error($"Could not find command '{cmd.Command}' from extension");
					return false;
				}
				rawCmd.Requires = cmd.Requires;
				cmdCount++;
				if (Args.Debug) {
					Debug($"  Found extension command '{cmd.Command}'");
				}
			}
		}

		if (Args.Verbose) {
			Info($"Found {enumCount} extension enum values");
			Info($"Found {cmdCount} extension commands");
		}

		return true;
	}

	private static RawExtension.EnumValue LoadCoreEnum(XmlNode enumNode)
	{
		var name = enumNode.Attributes!["name"]!.Value;
		var extend = enumNode.Attributes["extends"]!.Value;

		if (enumNode.Attributes["alias"] is XmlAttribute aliasAttr) {
			return new(null!, name, extend, 0, aliasAttr.Value);
		}
		else if (enumNode.Attributes["bitpos"] is XmlAttribute bpAttr) {
			return new(null!, name, extend, 1 << Int32.Parse(bpAttr.Value), null);
		}
		else if (enumNode.Attributes["offset"] is XmlAttribute offAttr) {
			var extNum = Int32.Parse(enumNode.Attributes["extnumber"]!.Value);
			var isNeg = (enumNode.Attributes["dir"]?.Value ?? String.Empty) == "-";
			var intval = 1_000_000_000 + ((extNum - 1) * 1000);
			var offset = Int32.Parse(offAttr.Value);
			intval += offset;
			return new(null!, name, extend, isNeg ? -intval : intval, null);
		}
		else if (enumNode.Attributes["value"] is XmlAttribute valueAttr) {
			return new(null!, name, extend, Int32.Parse(valueAttr.Value), null);
		}
		else {
			throw new NotImplementedException($"Cannot parse value for core enum {name}");
		}
	}
}
