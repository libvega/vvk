﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml;

namespace VVK;


// Unprocessed enum loaded from the spec
public class RawEnum
{
	// Record of a enum value within an enum type
	public record Value(string Name, string? Alias, int IntValue);

	#region Fields
	// The name of the enum type
	public readonly string Name;
	// If the type acts as flags
	public readonly bool IsFlags;
	// The bitwidth of the enum
	public uint Size { get; private set; } = 32;

	// List of values
	public readonly List<Value> Values = new();
	#endregion // Fields

	public RawEnum(string name, bool isFlags)
	{
		Name = name;
		IsFlags = isFlags;
	}

	public static bool TryParse(XmlNode xml, RawEnum @enum)
	{
#if !DEBUG
		try {
#endif
			Parse(xml, @enum);
			return true;
#if !DEBUG
		}
		catch (Exception ex) {
			Error(ex.Message);
			return false;
		}
#endif
	}

	private static void Parse(XmlNode xml, RawEnum @enum)
	{
		// Check size
		if (xml.Attributes!["bitwidth"] is XmlAttribute bwAttr) {
			@enum.Size = UInt32.Parse(bwAttr.Value);
		}

		// Load values
		if (xml.SelectNodes("enum") is XmlNodeList nodeList) {
			foreach (var node in nodeList.OfType<XmlNode>()) {
				var name = node.Attributes!["name"]!.Value;
				string? alias = null;
				int value = 0;
				if (node.Attributes["value"] is XmlAttribute valueAttr) {
					var ishex = valueAttr.Value.StartsWith("0x");
					value = Int32.Parse(ishex ? valueAttr.Value.Substring(2) : valueAttr.Value, 
						ishex ? NumberStyles.HexNumber : NumberStyles.Integer);
				}
				else if (node.Attributes["bitpos"] is XmlAttribute bitposAttr) {
					value = 1 << Int32.Parse(bitposAttr.Value);
				}
				else if (node.Attributes["alias"] is XmlAttribute aliasAttr) {
					alias = aliasAttr.Value;
				}
				else {
					throw new Exception($"Cannot identify value for enum {@enum.Name}::{name}");
				}
				@enum.Values.Add(new(name, alias, value));
			}
		}
	}
}
