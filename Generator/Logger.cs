﻿/*
 * MIT License (MIT) - Copyright (c) 2021 Sean Moss
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */


global using static VVK.Logger;
using System;

namespace VVK;


// Logging functionality with console colors
static class Logger
{
	private const ConsoleColor DebugColor = ConsoleColor.DarkGray;
	private const ConsoleColor WarningColor = ConsoleColor.Yellow;
	private const ConsoleColor ErrorColor = ConsoleColor.Red;

	public static void Info(string message) => Console.WriteLine(message);

	public static void Debug(string message)
	{
		Console.ForegroundColor = DebugColor;
		Console.WriteLine(message);
		Console.ResetColor();
	}

	public static void Warning(string message)
	{
		Console.ForegroundColor = WarningColor;
		Console.WriteLine(message);
		Console.ResetColor();
	}

	public static void Error(string message)
	{
		Console.ForegroundColor = ErrorColor;
		Console.WriteLine(message);
		Console.ResetColor();
	}
}
